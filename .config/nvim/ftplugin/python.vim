setlocal makeprg=python3\ %:S
noremap <buffer> RT :!python3 -m unittest<CR>

command! PyMain call append('.', [
			\ "def main():",
			\ "    pass",
			\ "",
			\ "",
			\ "if __name__ == '__main__':",
			\ "    main(*sys.argv[1:])"
			\ ])

command! PyMainWithCasts call append('.', [
			\ "import sys",
			\ "import functools",
			\ "",
			\ "def _cast_args(f):",
			\ "    @functools.wraps(f)",
			\ "    def wrapped(*args):",
			\ "        typed_args = (tpe(val) for tpe, val in",
			\ "                      zip(f.__annotations__.values(), args))",
			\ "        return f(*typed_args)",
			\ "    return wrapped",
			\ "",
			\ "@_cast_args",
			\ "def main():",
			\ "    pass",
			\ "",
			\ "",
			\ "if __name__ == '__main__':",
			\ "    main(*sys.argv[1:])"
			\ ])

command! IncVer !incversion.sh '/    version/' setup.py
