setlocal spelllang=cs
setlocal spell
setlocal makeprg=xelatex\ -halt-on-error\ -output-directory\ %:h:S\ %:S
setlocal commentstring=%\ %s
