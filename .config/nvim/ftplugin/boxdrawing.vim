" mnemonics: 
" 	West ╸, North ╹, East ╺, South ╻
" 	Horizontal ━, Vertical ┃
" 	R ┏, I ┓, L ┗, J ┛
" 	 F ┣, 4 ┫, T ┳, A ┻
" 	  X ╋

" WNES
abbreviate <buffer> ww ╴
abbreviate <buffer> nn ╵
abbreviate <buffer> ee ╶
abbreviate <buffer> ss ╷
abbreviate <buffer> WW ╸
abbreviate <buffer> NN ╹
abbreviate <buffer> EE ╺
abbreviate <buffer> SS ╻

" H
abbreviate <buffer> hh ─
abbreviate <buffer> wE ╼
abbreviate <buffer> Ew ╼
abbreviate <buffer> We ╾
abbreviate <buffer> eW ╾
abbreviate <buffer> HH ━

" V
abbreviate <buffer> vv │
abbreviate <buffer> nS ╽
abbreviate <buffer> Sn ╽
abbreviate <buffer> Ns ╿
abbreviate <buffer> sN ╿
abbreviate <buffer> VV ┃

" R
abbreviate <buffer> rr ┌

abbreviate <buffer> sE ┍
abbreviate <buffer> Es ┍
abbreviate <buffer> Se ┎
abbreviate <buffer> eS ┎

abbreviate <buffer> RR ┏

" I
abbreviate <buffer> ii ┐

abbreviate <buffer> sW ┑
abbreviate <buffer> Ws ┑
abbreviate <buffer> Sw ┒
abbreviate <buffer> wS ┒

abbreviate <buffer> II ┓

" L
abbreviate <buffer> ll └

abbreviate <buffer> nE ┕
abbreviate <buffer> En ┕
abbreviate <buffer> Ne ┖
abbreviate <buffer> eN ┖

abbreviate <buffer> LL ┗

" J
abbreviate <buffer> jj ┘

abbreviate <buffer> nW ┙
abbreviate <buffer> Wn ┙
abbreviate <buffer> Nw ┚
abbreviate <buffer> wN ┚

abbreviate <buffer> JJ ┛

" F
abbreviate <buffer> ff ├

abbreviate <buffer> Nr ┞
abbreviate <buffer> rN ┞
abbreviate <buffer> Ev ┝
abbreviate <buffer> vE ┝
abbreviate <buffer> Sl ┟
abbreviate <buffer> lS ┟

abbreviate <buffer> sL ┡
abbreviate <buffer> Ls ┡
abbreviate <buffer> eV ┠
abbreviate <buffer> Ve ┠
abbreviate <buffer> nR ┢
abbreviate <buffer> Rn ┢

abbreviate <buffer> FF ┣

" 4
abbreviate <buffer> 44 ┤

abbreviate <buffer> Wv ┥
abbreviate <buffer> vW ┥
abbreviate <buffer> Ni ┦
abbreviate <buffer> iN ┦
abbreviate <buffer> Sj ┧
abbreviate <buffer> jS ┧

abbreviate <buffer> sJ ┩
abbreviate <buffer> Js ┩
abbreviate <buffer> nI ┪
abbreviate <buffer> In ┪
abbreviate <buffer> wV ┨
abbreviate <buffer> Vw ┨

abbreviate <buffer> $$ ┫

" T
abbreviate <buffer> tt ┬

abbreviate <buffer> Wr ┭
abbreviate <buffer> rW ┭
abbreviate <buffer> Ei ┮
abbreviate <buffer> iE ┮
abbreviate <buffer> sH ┯
abbreviate <buffer> Hs ┯

abbreviate <buffer> Sh ┰
abbreviate <buffer> hS ┰
abbreviate <buffer> eI ┱
abbreviate <buffer> Ie ┱
abbreviate <buffer> wR ┲
abbreviate <buffer> Rw ┲

abbreviate <buffer> TT ┳

" A
abbreviate <buffer> aa ┴

abbreviate <buffer> Wl ┵
abbreviate <buffer> lW ┵
abbreviate <buffer> Nh ┸
abbreviate <buffer> hN ┸
abbreviate <buffer> Ej ┶
abbreviate <buffer> jE ┶

abbreviate <buffer> eJ ┹
abbreviate <buffer> Je ┹
abbreviate <buffer> nH ┷
abbreviate <buffer> Hn ┷
abbreviate <buffer> wL ┺
abbreviate <buffer> Lw ┺

abbreviate <buffer> AA ┻

" X
abbreviate <buffer> xx ┼

abbreviate <buffer> Wf ┽
abbreviate <buffer> fW ┽
abbreviate <buffer> Nt ╀
abbreviate <buffer> tN ╀
abbreviate <buffer> E4 ┾
abbreviate <buffer> 4E ┾
abbreviate <buffer> Sa ╁
abbreviate <buffer> aS ╁

abbreviate <buffer> hV ╂
abbreviate <buffer> Vh ╂
abbreviate <buffer> Jr ╃
abbreviate <buffer> rJ ╃
abbreviate <buffer> iL ╄
abbreviate <buffer> Li ╄
abbreviate <buffer> Il ╅
abbreviate <buffer> lI ╅
abbreviate <buffer> jR ╆
abbreviate <buffer> Rj ╆
abbreviate <buffer> Hv ┿
abbreviate <buffer> vH ┿

abbreviate <buffer> sA ╇
abbreviate <buffer> As ╇
abbreviate <buffer> e$ ╉
abbreviate <buffer> $e ╉
abbreviate <buffer> nT ╈
abbreviate <buffer> Tn ╈
abbreviate <buffer> wF ╊
abbreviate <buffer> Fw ╊

abbreviate <buffer> XX ╋
