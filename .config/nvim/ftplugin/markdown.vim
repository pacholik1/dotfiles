setlocal makeprg=md2html.py\ %:S
setlocal shiftwidth=4
setlocal tabstop=4
setlocal commentstring=>\ %s

let g:table_mode_corner='|'
" silent TableModeEnable

nnoremap <buffer> g= :put =substitute(getline('.'), '.', '=', 'g')<CR>
nnoremap <buffer> g- :put =substitute(getline('.'), '.', '-', 'g')<CR>
