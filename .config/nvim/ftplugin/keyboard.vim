set commentstring=//\ %s

" ‚ ‘ ’
abbreviate <buffer> '1 U201A
abbreviate <buffer> '2 U2018
abbreviate <buffer> '3 U2019
" „ “ ”
abbreviate <buffer> "1 U201E
abbreviate <buffer> "2 U201C
abbreviate <buffer> "3 U201D

" •
abbreviate <buffer> bullet U2022
" ₽ ₿
abbreviate <buffer> bitcoin U20BD
abbreviate <buffer> bitcoin U20BF
" ℮ ↯
abbreviate <buffer> estimated U212E
abbreviate <buffer> lightning U21AF
" █
abbreviate <buffer> block U2588

" ↵ ⇧ ⇥
abbreviate <buffer> enter U21B5
abbreviate <buffer> shift U21E7
" abbreviate <buffer> tab U21B9
abbreviate <buffer> tab U21E5
" ctrl ⎈  shift ⇧  ralt ⇮  tab ⇥  esc ⎋  bs ⌫  enter ↵
" insert ⎀  del ⌦  home ⇱  end ⇲  pgup ⇞  pgdn ⇟

" ☠ ⚡
abbreviate <buffer> skull U2620
abbreviate <buffer> thunder U26A1
" ♀ ♂
" abbreviate <buffer> male 2642
" abbreviate <buffer> female 2640
" ♠ ♣ ♥ ♦
abbreviate <buffer> spade U2660
" abbreviate <buffer> club U2663 
" abbreviate <buffer> heart U2665
" abbreviate <buffer> diamond U2666
" ♫ ♭ ♮ ♯
abbreviate <buffer> note U266B 
" abbreviate <buffer> flat U266D
" abbreviate <buffer> neutral U266E
" abbreviate <buffer> sharp U266F

" ❤️
abbreviate <buffer> heart U2764

" ✓ ✗
abbreviate <buffer> ok U2713
abbreviate <buffer> err U2717
" ⟨ ⟩
abbreviate <buffer> <( U27E8
abbreviate <buffer> )> U27E9
" ⧼ ⧽
abbreviate <buffer> <( U29FC
abbreviate <buffer> )> U29FD

" ☭ 卐
abbreviate <buffer> srp U+262D
abbreviate <buffer> swastika U5350
" ✝ ✡ ☪︎ 
abbreviate <buffer> cross U271D
abbreviate <buffer> jew U2721
abbreviate <buffer> islam U262A

" 𝄞 𝄡 𝄢
abbreviate <buffer> gclef U1D11E
abbreviate <buffer> cclef U1D121
abbreviate <buffer> fclef U1D122

" 👍 👎
abbreviate <buffer> thumbsup U1F44D 
abbreviate <buffer> thumbsdown U1F44E 
" 👌 🤘 🖕 ✌
abbreviate <buffer> okhand U1F44C
abbreviate <buffer> \m/ U1F918
abbreviate <buffer> middlefinger U1F595
abbreviate <buffer> victory U270C

" 🍺 🍷 🐓
abbreviate <buffer> beer U1F37A
abbreviate <buffer> wine U1F377
abbreviate <buffer> cock U1F413

" " ☹
" abbreviate <buffer> :( U2639
" 😕
abbreviate <buffer> :( U1F615
" " ☺
" abbreviate <buffer> :) U263A
" 🙂
abbreviate <buffer> :) U1F642
" 😀
abbreviate <buffer> grin U1F600
" 😃
abbreviate <buffer> :D U1F603
" 😉
abbreviate <buffer> ;) U1F609
" 😊
abbreviate <buffer> ^^ U1F60A
" 😎
abbreviate <buffer> 8) U1F60E
" 😐
abbreviate <buffer> :\| U1F610
" 😕
abbreviate <buffer> :/ U1F615
" 😗
abbreviate <buffer> :* U1F617
" 😛
abbreviate <buffer> :P U1F61B
" 😮
abbreviate <buffer> :o U1F62E
" 😲
abbreviate <buffer> :O U1F632
" 😴
abbreviate <buffer> zzz U1F634
" 🙄
abbreviate <buffer> rolling U1F644
" 🤔
abbreviate <buffer> think U1F914
" 🤢
abbreviate <buffer> nausea U1F922
" 🤣
abbreviate <buffer> rofl U1F923
" 🤮
abbreviate <buffer> puke U1F92E
" 😏
abbreviate <buffer> smirk U1F60F
" 🤫
abbreviate <buffer> pst U1F92B
" 😋
abbreviate <buffer> yum U1F60B

" 🤦 🤷 🙋 ✋ 
abbreviate <buffer> facepalm U1F926
abbreviate <buffer> shrug U1F937
abbreviate <buffer> hand U270B
abbreviate <buffer> raised U1F64B
" 👻
abbreviate <buffer> ghost U1F47B

" ツ
abbreviate <buffer> tsu U30C4

" unicode --format=U{ordc:X}
abbreviate <buffer> aflag U1F1E6
abbreviate <buffer> bflag U1F1E7
abbreviate <buffer> cflag U1F1E8
abbreviate <buffer> dflag U1F1E9
abbreviate <buffer> eflag U1F1EA
abbreviate <buffer> fflag U1F1EB
abbreviate <buffer> gflag U1F1EC
abbreviate <buffer> hflag U1F1ED
abbreviate <buffer> iflag U1F1EE
abbreviate <buffer> jflag U1F1EF
abbreviate <buffer> kflag U1F1F0
abbreviate <buffer> lflag U1F1F1
abbreviate <buffer> mflag U1F1F2
abbreviate <buffer> nflag U1F1F3
abbreviate <buffer> oflag U1F1F4
abbreviate <buffer> pflag U1F1F5
abbreviate <buffer> qflag U1F1F6
abbreviate <buffer> rflag U1F1F7
abbreviate <buffer> sflag U1F1F8
abbreviate <buffer> tflag U1F1F9
abbreviate <buffer> uflag U1F1FA
abbreviate <buffer> vflag U1F1FB
abbreviate <buffer> wflag U1F1FC
abbreviate <buffer> xflag U1F1FD
abbreviate <buffer> yflag U1F1FE
abbreviate <buffer> zflag U1F1FF
