" SYNTAX
set number
syntax on
filetype plugin indent on
set autoindent
set modeline
syntax sync minlines=10000
" syntax sync fromstart

" COMPLETION
let g:SuperTabDefaultCompletionType = "<c-n>"

" " Syntastic
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0
" let g:syntastic_python_python_exec = 'python3'
" let g:syntastic_cs_checkers = ['code_checker']

" ALE
let g:ale_open_list = 1
" let g:ale_set_loclist = 1
" let g:ale_set_quickfix = 1

" " COC
" let g:coc_node_path = '/usr/bin/node'
" let g:coc_global_extensions = [
" 			\ 'coc-json',
" 			\ 'coc-git', 'coc-sh',
" 			\ 'coc-pyright',
" 			\ 'coc-go',
" 			\ 'coc-html',
" 			\ 'coc-tsserver', 'coc-angular', '@yaegassy/coc-volar',
" 			\ 'coc-css', '@yaegassy/coc-tailwindcss3',
" 			\ 'coc-omnisharp',
" 			\ 'coc-java',
" 			\ 'coc-lua',
" 			\ 'coc-vimlsp',
" 			\ 'coc-texlab',
" 			\ 'coc-sql', 'coc-db',
" 			\ 'coc-solargraph',
" 			\ 'coc-calc',
" 			\ ]

" close the preview window once a completion has been inserted
augroup ClosePreview
	autocmd!
	autocmd CompleteDone * if !pumvisible() | pclose | endif
augroup END

" EYE CANDY
" colorscheme gruvbox	" at the end
let g:gruvbox_italic = 1
let g:gruvbox_italicize_strings = 0
set background=dark
set linespace=-2
" no icons or menu, no scrollbars, console dialogs
set guioptions=ac
set guifont=DejaVu\ Sans\ Mono\ for\ Powerline
set guicursor+=n-v-c:blinkon0
set termguicolors
set colorcolumn=100
set hlsearch incsearch
" changing cursor in terminal
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"
" neovim features
set inccommand=nosplit

" STATUSLINE
set showcmd					" show current command
set wildmenu wildmode=list:longest,full		" ex mode completion
" buffer list
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#ale#enabled = 1
let g:airline_powerline_fonts = 1
set laststatus=2

" BEHAVIOUR
set linebreak
set display+=lastline		" don't show @ when line doesn't fit
set mouse=a			" mouse scroll
set lazyredraw			" do not redraw screen inside macros
set clipboard=unnamedplus	" always use clipboard
" set autochdir
augroup ProjectRoot
	autocmd!
	autocmd BufEnter * ProjectRootCD
augroup END
augroup grep
	autocmd!
	autocmd QuickFixCmdPost *grep* cwindow	" don't close quickfix on grep
augroup END
set nrformats=hex	" no oct: 07 -> 10
set autowriteall	" autosaving
" set hidden		" undo histrory saved
augroup hidden
	autocmd!
	autocmd BufReadPost * set bufhidden=hide
	autocmd BufWinEnter quickfix setlocal bufhidden=wipe nobuflisted noswapfile
augroup END
set directory=/tmp	" swap files in /tmp
augroup noh
	autocmd!
	autocmd InsertEnter * let b:_search=@/|let @/=''
	autocmd InsertLeave * let @/=get(b:,'_search','')|nohlsearch|redraw
augroup END

" FOLDING
" set foldignore=
" set foldmethod=marker
set foldlevelstart=99
" let g:SimpylFold_docstring_preview = 1
" let g:SimpylFold_fold_docstring	= 0
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()

" PLUGIN SETTINGS
let g:licenses_copyright_holders_name = 'Pachol, Vojtěch <pacholick@gmail.com>'

let g:vimpager = {}
let g:less     = {}
let g:vimpager.X11 = 0

" let g:ai_timeout=10
let g:ai_no_mappings=1
let g:vim_ai_chat = {
\  "options": {
\    "model": "gpt-4",
\  },
\}

" MAPPINGS, INCLUDES
runtime include.vim
runtime mappings.vim

silent execute '!~/.config/nvim/bundle/gruvbox/gruvbox_256palette.sh'
" let g:gruvbox_material_background = 'hard'
colorscheme gruvbox8

lua <<EOF
-- Use the `default_options` as the second parameter, which uses
-- `foreground` for every mode. This is the inverse of the previous
-- setup configuration.
require 'colorizer'.setup {
	'*'; -- Highlight all files, but customize some others.
	css = {
		RRGGBBAA = true;	-- #RRGGBBAA hex codes
		rgb_fn = true;	        -- CSS rgb() and rgba() functions
		-- css = true;       -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
		};
	html = {  } -- Disable parsing "names" like Blue or Gray
	}
EOF
