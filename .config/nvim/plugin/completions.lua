-- if not vim.g.modifiable then
-- 	return
-- end
local os = require'os'
local cmp = require'cmp'
local capabilities = require("cmp_nvim_lsp").default_capabilities()
local lspconfig = require 'lspconfig'
local luasnip = require 'luasnip'
require'cmp_git'.setup()
-- require'codeium'.setup{}
require'cmp-npm'.setup({})

require("copilot").setup({
  suggestion = { enabled = false },
  panel = { enabled = false },
})
require("copilot_cmp").setup()
require("CopilotChat").setup {
  debug = true, -- Enable debugging
  -- See Configuration section for rest
}

local servers = {
	-- pip3 install pyright django-template-lsp
	'pyright', 'djlsp',
	-- npm typescript-language-server @tailwindcss/language-server
	'ts_ls', 'html', 'cssls', 'jsonls', 'tailwindcss', 'astro', 'svelte',
	-- npm i bash-language-server
	'bashls',
	-- npm i vim-language-server
	'vimls',
	'jdtls',
}
for _, lsp in ipairs(servers) do
	lspconfig[lsp].setup {
		-- on_attach = my_custom_on_attach,
		capabilities = capabilities,
	}
end
lspconfig.jdtls.setup {
	cmd = { os.getenv('HOME') ..
		'/.config/nvim/bundle/nvim-jdtls/jdt-language-server-latest/bin/jdtls' },
}

cmp.setup({
	snippet = {
		-- REQUIRED - you must specify a snippet engine
		expand = function(args)
			--   vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
			require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
			-- require('snippy').expand_snippet(args.body) -- For `snippy` users.
			-- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
		end,
	},
	window = {
		completion = cmp.config.window.bordered(),
		documentation = cmp.config.window.bordered(),
	},
	mapping = cmp.mapping.preset.insert({
		-- ['<C-b>'] = cmp.mapping.scroll_docs(-4),
		-- ['<C-f>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete(),
		-- ['<C-e>'] = cmp.mapping.abort(),
		['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
		['<Tab>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			else
				fallback()
			end
		end, { 'i', 's' }),
		['<S-Tab>'] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end, { 'i', 's' }),
	}),
	sources = cmp.config.sources({
		{ name = 'nvim_lsp' },
		-- { name = 'codeium' },
		{ name = 'copilot' },
		{ name = 'calc' },
		{ name = 'cmp-tw2css' },
		{ name = 'scss' },
		{ name = 'path', option = { trailing_slash = true }},
		{ name = 'git' },
		{ name = 'npm', keyword_length = 4 },
	}, {
		{ name = 'buffer' },
	})
})
