nnoremap <LocalLeader>m :set operatorfunc=<SID>MongoRun<CR>g@
nnoremap <LocalLeader>mm :set operatorfunc=<SID>MongoRun<CR>g@_
xnoremap <LocalLeader>m :<C-U>call <SID>MongoRun(visualmode(), 1)<CR>
" TODO:
" work with ranges
let g:mongorun_dbaddress = 'FareOnEshop'

function! s:MongoRun(type, ...) range abort
	" let sel_save = &selection
	" let &selection = "inclusive"
	" let reg_save = @@

	if a:0  " Invoked from Visual mode, use gv command.
		silent normal! gv"zy
	" elseif a:firstline
	" 	silent execute a:firstline . ',' . a:lastline . 'yank z'
	elseif a:type == 'line'
		silent normal! '[V']"zy
	else
		silent normal! `[v`]"zy
	endif

	botright new
	setlocal buftype=nofile bufhidden=wipe nobuflisted noswapfile filetype=javascript
	silent execute '$read !mongo ' . g:mongorun_dbaddress . ' --eval ' . shellescape(@z, 1)

	" let &selection = sel_save
	" let @@ = reg_save
endfunction
