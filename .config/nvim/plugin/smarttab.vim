function! Smart_TabComplete() abort
	let line = getline('.')					" current line

	let substr = strpart(line, -1, col('.')+1)		" from the start of the current
	" line to one character right
	" of the cursor
	let substr = matchstr(substr, "[^ \t]*$")		" word till cursor
	if (strlen(substr)==0)					" nothing to match on empty string
		return "\<tab>"
	endif
	" let has_period = match(substr, '\.') != -1		" position of period, if any
	let has_slash = match(substr, '\/') != -1		" position of slash, if any
	" if (!has_period && !has_slash)
	"   return "\<C-X>\<C-P>"				" existing text matching
	if has_slash
		return "\<C-X>\<C-F>"				" file matching
	else
		return "\<C-X>\<C-O>"				" plugin matching
	endif
endfunction

" inoremap <Tab> <C-r>=Smart_TabComplete()<CR>


" https://github.com/neoclide/coc.nvim/wiki/Completion-with-sources#use-tab-or-custom-key-for-trigger-completion
" use <tab> for trigger completion and navigate to the next complete item
function! CheckBackspace() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" inoremap <silent><expr> <Tab>
" 			\ coc#pum#visible() ? coc#pum#next(1) :
" 			\ CheckBackspace() ? "\<Tab>" :
" 			\ coc#refresh()
" " inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"
" inoremap <silent><expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"
