" emacs bindings
" insert mode only, with few exceptions if not clashing and useful

" MOVEMENT
noremap! <C-b> <Left>
noremap! <C-f> <Right>
noremap! <C-a> <Home>
noremap <C-e> <End>
noremap! <C-e> <End>
noremap! <M-b> <C-Left>
noremap! <M-B> <C-Left>
noremap! <M-f> <C-Right>
noremap! <M-F> <C-Right>

" DELETION
noremap! <C-h> <BS>
noremap! <C-d> <Del>
noremap! <M-BS> <C-w>
inoremap <M-d> <C-r>=emacs#kill_word()<CR>
cnoremap <M-d> <C-Right><C-w>
inoremap <C-k> <C-r>=emacs#kill_line()<CR>
" cnoremap <C-k> <C-f>D<C-c><C-c>:<Up>
noremap <M-\> :call emacs#delete_horizontal_space()<CR>
inoremap <M-\> <C-r>=emacs#delete_horizontal_space()<CR>
" noremap <M-Space> :call emacs#just_one_space()<CR>
" inoremap <M-Space> <C-r>=emacs#just_one_space()<CR>

" YANKING (pasting in emacsese)
noremap! <C-y> <C-r>"

" TRANSPOSING
inoremap <C-t> <C-r>=emacs#transpose_chars()<CR>
" noremap! <M-t>	– Transpose two words
" noremap! <C-M-t>	– Transpose two balanced expressions

" asdf        ghjk
