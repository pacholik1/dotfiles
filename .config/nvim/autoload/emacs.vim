" emacs bindings
" especialy in insert mode, but in normal too if useful and not clashing

" Kill rest of line or one or more lines
function! emacs#kill_line() abort
	let line = getline('.')
	let c = col('.')-1

	if c == strlen(line)
		normal! J
	else
		call setline('.', strpart(line, 0, c))
	endif
	return ''
endfunction

" Kill forward to the end of the next word
function! emacs#kill_word() abort
	" let line = getline('.')
	" let c = col('.')-1

	" let before_cursor = strpart(line, 0, c)
	" let after_cursor = strpart(line, c)

	" if match(after_cursor, '\w') == -1
	" 	let after_cursor = ''
	" else
	" 	" let after_cursor = substitute(after_cursor, '^.\{-}\w\ze\W', '', '')
	" 	let after_cursor = substitute(after_cursor, '^.\{-}\>', '', '')
	" endif

	" call setline('.', before_cursor . after_cursor)
	normal! dw
	return ''
endfunction

" Delete spaces and tabs around point
function! emacs#delete_horizontal_space() abort
	let line = getline('.')
	let c = col('.')-1

	let before_cursor = substitute(strpart(line, 0, c), '\s\+$', '', '')
	let after_cursor = substitute(strpart(line, c), '^\s\+', '', '')

	call setline('.', before_cursor . after_cursor)
	call cursor('.', strlen(before_cursor)+1)
	return ''
endfunction

" Delete spaces and tabs around point, leaving one space
function! emacs#just_one_space() abort
	call emacs#delete_horizontal_space()
	return ' '
endfunction

" Transpose two characters
function! emacs#transpose_chars() abort
	let line = getline('.')
	let c = col('.')-1

	if c == strlen(line)
		let c = c-1
	endif

	let leading = strpart(line, 0, c-1)
	let a = strpart(line, c-1, 1)
	let b = strpart(line, c, 1)
	let trailing = strpart(line, c+1)

	call setline('.', leading . b . a . trailing)
	call cursor('.', col('.')+1)
	return ''
endfunction

" Transpose two words
function! emacs#transpose_words() abort
	" TODO
	let line = getline('.')
	let c = col('.')-1

	return ''
endfunction

" Transpose two balanced expressions
function! emacs#transpose_sexps() abort
	" TODO
	let line = getline('.')
	let c = col('.')-1

	return ''
endfunction
