function! motions#whole_file_motion()
	" https://vi.stackexchange.com/a/24856/2597
	let b:view_dict = winsaveview()
	normal! ggVG
	call feedkeys("\<Plug>(RestoreView)")
endfunction

nnoremap <silent> <Plug>(RestoreView) :call winrestview(b:view_dict)<CR>
