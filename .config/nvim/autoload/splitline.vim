" https://vi.stackexchange.com/a/28952/2597

" function! splitline#splitline(type) range abort
" 	if a:type == 'command'
" 		let l:from  = a:000[0]
" 		let l:to   = a:000[1]
" 	elseif a:type == 'visual'
" 		let l:from  = line("'<")
" 		let l:to   = line("'>")
" 	elseif a:type =~ 'line\|char\|block'
" 		" Test the different mode that g@ can pass as argument
" 		" and get the corresponding lines
" 		let l:from  = line("'[")
" 		let l:to   = line("']")
" 	endif

" 	execute l:from . ',' . l:to . 's/\>\s*\</\r/g'
" 	let @/ = @"
" 	normal =''
" endfunction

function! splitline#splitline(type) range abort
	execute line("'[") . ',' . line("']") . 's/\s\+/\r/g'
	let @/ = @"
	normal =''
endfunction
