call plug#begin('~/.config/nvim/bundle')
" COMPLETIONS
" Plug 'rdnetto/YCM-Generator', { 'branch': 'stable', 'for': ['c', 'cpp'] }
" --clang-completer --system-boost --system-libclang
" Plug 'Valloric/YouCompleteMe', { 
" 			\'do': 'python3 install.py --clang-completer --system-boost --system-libclang --cs-completer --ts-completer',
" 			\'for': ['c', 'cpp', 'python', 'go', 'typescript', 'javascript', 'rust'] }
" Plug 'neoclide/coc.nvim', { 'branch': 'release' }
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/nvim-cmp'				" Autocompletion plugin
Plug 'hrsh7th/cmp-nvim-lsp'			" LSP source for nvim-cmp
Plug 'L3MON4D3/LuaSnip', {'tag': 'v2.*', 'do': 'make install_jsregexp'} " Replace <CurrentMajor> by the latest released major (first number of latest release)
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-calc'
Plug 'jcha0713/cmp-tw2css'
Plug 'mmolhoek/cmp-scss'
Plug 'petertriho/cmp-git'
Plug 'David-Kunz/cmp-npm'
" Plug 'maan2003/lsp_lines.nvim'			" diagnostics using virtual lines

" LANGUAGE SUPPORT
" WEB
Plug 'norcalli/nvim-colorizer.lua'
Plug 'kevinoid/vim-jsonc'
" Plug 'HerringtonDarkholme/yats.vim', { 'for': ['typescript', 'typescriptreact'] }
" Plug 'MaxMEllon/vim-jsx-pretty', { 'for': [ 'jsx', 'typescript', 'typescriptreact' ] }
" Plug 'posva/vim-vue', { 'for': 'vue' }			" vue
Plug 'storyn26383/vim-vue', { 'for': 'vue' }			" vue
" Plug 'leafOfTree/vim-vue-plugin'
Plug 'wuelnerdotexe/vim-astro'
Plug 'evanleck/vim-svelte', {'branch': 'main'}
" PYTHON
" Plug 'nvie/vim-flake8', { 'for': 'python' }			" PEP8 check
Plug 'hynek/vim-python-pep8-indent', { 'for': 'python' }
" Plug 'Konfekt/FastFold'
" Plug 'tmhedberg/SimpylFold', { 'for': 'python' }
" GO
Plug 'fatih/vim-go', { 'for': 'go' }
" LUA
" Plug 'hrsh7th/cmp-nvim-lua'
" JAVA
Plug 'mfussenegger/nvim-jdtls', { 'for': 'java' }

" DOCS
" Plug 'gerw/vim-latex-suite', { 'for': 'tex' }
" Plug 'lervag/vimtex', { 'for': 'tex' }
Plug '/usr/share/lilypond/2.22.0/vim/', { 'for': 'lilypond' }
" Plug 'dbakker/vim-rstlink', { 'for': 'rst' }
" Plug 'greyblake/vim-preview', { 'for': 'markdown' }
" Plug 'suan/vim-instant-markdown', { 'for': 'markdown' }
" Plug 'torkve/vim-markdown-extra-preview', { 'for': 'markdown' }
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }

" VIM
Plug 'tpope/vim-scriptease', { 'for': 'vim' }	" for vim plugins
Plug 'Shougo/neco-vim', { 'for': 'vim' }	" viml completion
" DB
" Plug 'vim-scripts/dbext.vim', { 'for': 'sql' }	" db access
Plug 'tpope/vim-dadbod'
Plug 'kristijanhusak/vim-dadbod-completion', { 'for': 'sql' }	" db completion
" PS
" Plug 'PProvost/vim-ps1', { 'for': 'ps1' }	" powershell highlighting
" CSV, TSV
Plug 'mechatroner/rainbow_csv'

" COMMENTING
Plug 'tpope/vim-commentary'			" `gcc`
Plug 'Shougo/context_filetype.vim'

" SYNTAX
Plug 'dense-analysis/ale'			" syntax checking
Plug 'andymass/vim-matchup'			" %
Plug 'tpope/vim-surround'			" quoting
" Plug 'ervandew/supertab'			" `<tab>`
Plug 'tpope/vim-sleuth'				" adjust shiftwidth
Plug 'christoomey/vim-titlecase'		" gz
Plug 'tpope/vim-abolish'                        " convert cases (crs)
Plug 'tommcdo/vim-lion'				" align `glip=`
Plug 'machakann/vim-highlightedyank' 		" highlight yanked
Plug 'farmergreg/vim-lastplace' 		" place cursor on new buffer
" TREESITTER
Plug 'nvim-treesitter/nvim-treesitter', {'tag': 'v0.9.3', 'do': ':TSUpdate'}
Plug 'nvim-treesitter/nvim-treesitter-textobjects'
Plug 'nvim-treesitter/nvim-treesitter-context'
Plug 'windwp/nvim-ts-autotag'			" tag close and rename

" IMAGES
Plug 'tpope/vim-afterimage'

" VCS
Plug 'tpope/vim-fugitive'			" git
Plug 'dbakker/vim-projectroot'
Plug 'lewis6991/gitsigns.nvim'
Plug 'Yggdroot/LeaderF', { 'do': ':LeaderfInstallCExtension' }

" COMMANDS
" Plug 'tpope/vim-unimpaired'			" ][
Plug 'tpope/vim-eunuch'				" shell commands
Plug 'antoyo/vim-licenses'
Plug 'dhruvasagar/vim-table-mode'
Plug 'vim-scripts/BufOnly.vim'
Plug 'tpope/vim-repeat'				" . works for plugins
Plug 'tpope/vim-speeddating'
" Plug 'christoomey/vim-sort-motion'		" sorting with gs
Plug 'machakann/vim-swap' 			" swap arguments
Plug 'tommcdo/vim-exchange'			" exchange motion, cxaw

" AI
" Plug 'Exafunction/codeium.vim'
Plug 'nvim-lua/plenary.nvim',
" Plug 'jcdickinson/codeium.nvim'
" Plug 'Exafunction/codeium.nvim'
" Plug 'github/copilot.vim'
Plug 'zbirenbaum/copilot.lua'
Plug 'zbirenbaum/copilot-cmp'
Plug 'CopilotC-Nvim/CopilotChat.nvim'

" LOOK AND FEEL
" Plug 'morhetz/gruvbox'
" Plug 'gruvbox-community/gruvbox'		" color theme
Plug 'lifepillar/vim-gruvbox8', {'branch': 'neovim'}
" Plug 'sainnhe/gruvbox-material'
" Plug 'luisiacc/gruvbox-baby'
" Plug 'ellisonleao/gruvbox.nvim', {'tag': '1.0.0'}

Plug 'vim-airline/vim-airline'
Plug 'osyo-manga/vim-anzu'			" search position
Plug 'junegunn/vim-peekaboo'			" show content of registers
" Plug 'johngrib/vim-game-code-break'		" arkanoid
Plug 'karb94/neoscroll.nvim', {'commit': '71c8fad'}			" smooth scrolling
" Plug 'MunifTanjim/nui.nvim'
Plug 'echasnovski/mini.ai'

call plug#end()
lua require'colorizer'.setup()
