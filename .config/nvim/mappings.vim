" <Leader> with comma, <LocalLeader> with backslash
let mapleader = ','
let maplocalleader = '\'

" free keys:
" =		!+
" y[] 		Q
" dg		
" zc		Z

" BASICS {{{1
noremap ; :
noremap : ;
noremap Y y$
nnoremap <silent> <ESC> :cclose<BAR>nohlsearch<CR>
map Q <nop>
map R <nop>
map U <nop>

" MOVING AROUND {{{1
" " neoscroll does that

" TABS {{{1
noremap ZZ :update<BAR>bdelete<CR>
noremap ZA :%update<BAR>%bdelete<CR>
noremap ZO :%update<BAR>BufOnly<CR>
noremap ZQ :bdelete!<CR>

noremap <C-k> :bprevious<CR>
noremap <C-j> :bnext<CR>
noremap ó :bprevious<CR>
noremap π :bnext<CR>
noremap <C-ESC> :buffer#<CR>
noremap <C-w> <C-w><C-w>

nmap █ <Plug>AirlineSelectTab1
nmap ² <Plug>AirlineSelectTab2
nmap ³ <Plug>AirlineSelectTab3
nmap ⁴ <Plug>AirlineSelectTab4
nmap ½ <Plug>AirlineSelectTab5
nmap 😉 <Plug>AirlineSelectTab6
nmap 😃 <Plug>AirlineSelectTab7
nmap 😎 <Plug>AirlineSelectTab8
nmap 😕 <Plug>AirlineSelectTab9

" SAVING {{{1
noremap <LocalLeader>w :wall<CR>
" OPENING {{{1
noremap <expr> <LocalLeader>e ':e ' . expand("%:h") . '/'
noremap <expr> <LocalLeader>u ':e ' . expand("%:h") . '/../'
noremap <expr> <LocalLeader>U ':e ' . projectroot#guess() . '/'
noremap <LocalLeader>h :e ~/
noremap <LocalLeader>t :e /tmp/
noremap <LocalLeader>r :e /
" open related files
noremap <expr> <LocalLeader>Ej ':e ' . expand("%:r") . ".js\<CR>"
noremap <expr> <LocalLeader>Es ':e ' . expand("%:r") . ".scss\<CR>"
noremap <expr> <LocalLeader>Et ':e ' . expand("%:r") . "-tw-styles.js\<CR>"
noremap <expr> <LocalLeader>Eq ':e ' . expand("%:r") . ".query.js\<CR>"

" PASTING {{{1
noremap <silent> <Leader>p :put =@+<CR>
noremap <silent> <Leader>P :put! =@+<CR>
noremap <silent> <LocalLeader>p :put =@*<CR>
noremap <silent> <LocalLeader>P :put! =@*<CR>
" noremap! \p <C-r>=@*<CR>
" noremap! \P <C-r>=@*<CR>
noremap <Leader>y "+y
noremap <Leader>Y "+y$
noremap <LocalLeader>y "*y
noremap <LocalLeader>Y "*y$

" CREDENTIALS {{{1
nnoremap <silent>Q@ "=trim(system('git config --get user.email'))<CR>p
nnoremap <silent>QM "=trim(system('git config --get user.email'))<CR>p
nnoremap <silent>QN "=trim(system('git config --get user.name'))<CR>p

" SEARCHING {{{1
noremap / /\v\c
noremap ? ?\v\c
" search for visual selection			with \V you only have to escape \ and /
xnoremap * "zy/\V<C-r>=escape(@z, '\/')<CR><CR>
" xnoremap * "zy:let @/='\V' . escape(@z, '\/')<CR>n
xnoremap # "zy?\V<C-r>=escape(@z, '\?')<CR><CR>
" xnoremap # "zy:let @/='\V' . escape(@z, '\?')<CR>?<CR>
" open a Quickfix window for the last search
noremap <silent> <LocalLeader>/ :vimgrep /<C-r>//g %<CR>:copen<CR>

" REPLACING {{{1
noremap <LocalLeader>s :%s/\v
" replace every occurence
nnoremap Re :%s/\<<C-r><C-w>\>//gc<Left><Left><Left>
nnoremap RE :%s/\<<C-r><C-w>\>/<C-r><C-w>/gc<Left><Left><Left>
xnoremap Re "zy:%s/\V<C-r>=escape(@z, '\/')<CR>//gc<Left><Left><Left>
xnoremap RE "zy:%s/\V<C-r>=escape(@z, '\/')<CR>/<C-r>=escape(@z, '\/')<CR>/gc<Left><Left><Left>
" replace with lowercase and uppercase
nnoremap Ru :%s/\<<C-r><C-w>\>/\L&/gc<Left><Left><Left>
nnoremap RU :%s/\<<C-r><C-w>\>/\U&/gc<Left><Left><Left>
xnoremap Ru "zy:%s/\V<C-r>=escape(@z, '\/')<CR>/\L&/gc<Left><Left><Left>
xnoremap RU "zy:%s/\V<C-r>=escape(@z, '\/')<CR>/\U&/gc<Left><Left><Left>
" replace last search
noremap Rs :%s///gc<Left><Left><Left>
noremap RS :%s//<C-r>=escape(substitute(@z, '^\(\\.\)\+', '', ''), '\/')<CR>/gc<Left><Left><Left>
" split line
nnoremap RJJ :s/\s\+/\r/g<BAR>nohlsearch<CR>=''
xnoremap RJ :s/\s\+/\r/g<BAR>nohlsearch<CR>=''
nnoremap RJ :set opfunc=splitline#splitline<CR>g@

" " COMMENTING
" noremap gc <Plug>(caw:prefix)
" noremap gcc <Plug>(caw:hatpos:toggle:operator)

" FOLDING {{{1
noremap <expr> <CR> v:count > 0 ? "gg" : foldlevel('.') ? "za" : "\<CR>"
noremap <expr> <S-CR> &foldlevel ? "zMzv" : "zR"

" FUGITIVE SHORTCUTS {{{1
noremap <Leader>gc :Git<CR>
noremap <Leader>gs :Git<CR>
noremap <Leader>gd :Gdiff<CR>
noremap <Leader>gg :Ggrep 
noremap <Leader>gp :Git push<CR>
noremap <Leader>gl :Gclog %<CR>
noremap <Leader>gb :Git blame<CR>
" noremap <Leader>gpl :Gpull<CR>
" noremap <Leader>gw :Gwrite<CR>
" noremap <Leader>gi :CocCommand git.chunkInfo<CR>
" noremap <Leader>gu :CocCommand git.chunkUndo<CR>

" RUNNING {{{1
noremap RR :make<BAR>copen<CR>
noremap RI :IncVer<CR>

" COPILOT {{{1
noremap <Leader>cc :CopilotChatToggle<CR>
noremap <Leader>ce :CopilotChatExplain<CR>
noremap <Leader>cr :CopilotChatReview<CR>
noremap <Leader>cf :CopilotChatFix<CR>
noremap <Leader>cd :CopilotChatDocs<CR>
noremap <Leader>cm :CopilotChatCommit<CR>

" VISUAL
" stay in visual mode when indenting
xnoremap < <gv
xnoremap > >gv

" OPERATOR MAPPINGS {{{1
onoremap q i'
onoremap Q i"
" whole buffer
onoremap F :<C-u>call motions#whole_file_motion()<CR>
" outer files (not really operator mapping, but who cares)
" noremap yot :let @+ = join(readfile(join([expand("%:h"), "/../template.url"], "")))<CR>
" noremap yoo :let @+ = join(readfile(join([expand("%:h"), "/../overlay.url"], "")))<CR>
" noremap yoh :let @+ = join(
" 			\ readfile(join([expand("%:h"), "/index.html"], "")),
" 			\ "\n")<CR>
" noremap yoj :let @+ = join(
" 			\ readfile(join([expand("%:h"), "/script.js"], "")),
" 			\ "\n")<CR>
" noremap yos :let @+ = join(
" 			\ readfile(join([expand("%:h"), "/styles.css"], "")) +
" 			\ readfile(join([expand("%:h"), "/error-translations.css"], "")),
" 			\ "\n")<CR>

" DIGRAPHS {{{1
inoremap <C-\> <C-k>

" CALCULATE EQUATIONS {{{1
" inoremap <C-=> <C-o>"zyaW<End>=<C-r>=<C-r>z<CR>

" Use K to show documentation in preview window.
" nnoremap <silent> K :call docs#show_documentation()<CR>

" omap ih <Plug>(GitGutterTextObjectInnerPending)
" omap ah <Plug>(GitGutterTextObjectOuterPending)
" xmap ih <Plug>(GitGutterTextObjectInnerVisual)
" xmap ah <Plug>(GitGutterTextObjectOuterVisual)

" " SUPPRESS LATEX SUITE MAPPINGS {{{1
" imap <nop> <Plug>IMAP_JumpForward
" nmap <nop> <Plug>IMAP_JumpForward
" vmap <nop> <Plug>IMAP_JumpForward
" vmap <nop> <Plug>IMAP_DeleteAndJumpForward

lua <<EOF
local opts = {}
vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
-- vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
-- vim.keymap.set('n', '<Leader>wa', vim.lsp.buf.add_workspace_folder, opts)
-- vim.keymap.set('n', '<Leader>wr', vim.lsp.buf.remove_workspace_folder, opts)
-- vim.keymap.set('n', '<Leader>wl', function()
-- print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
-- end, opts)
-- vim.keymap.set('n', '<Leader>D', vim.lsp.buf.type_definition, opts)
vim.keymap.set('n', '<Leader>r', vim.lsp.buf.rename, opts)
vim.keymap.set({ 'n', 'v' }, '<Leader>ca', vim.lsp.buf.code_action, opts)
vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
vim.keymap.set('n', '<Leader>f', function()
vim.lsp.buf.format { async = true }
end, opts)
EOF
