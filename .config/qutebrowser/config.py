# pylint: disable=C0111
from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer  # noqa: F401
config = config  # type: ConfigAPI # noqa: F821 pylint: disable=E0602,C0103
c = c  # type: ConfigContainer # noqa: F821 pylint: disable=E0602,C0103

import os               # noqa: E402
import utils            # noqa: E402
# import qutenyan         # noqa: F401, E402
import base16           # noqa: F401, E402
try:
    import sugsearch    # noqa: F401
except ValueError:
    """Probably already registered."""

os.chdir('/tmp')

# SETTINGS
c.aliases.update({
    'spell-cs': '''set spellcheck.languages "['cs-CZ']"''',
    'spell-en': '''set spellcheck.languages "['en-US']"''',
    'spell-de': '''set spellcheck.languages "['de-DE']"''',
    'spell-es': '''set spellcheck.languages "['es-ES']"''',
    'spell-ru': '''set spellcheck.languages "['ru-RU']"''',
})

c.auto_save.session = True
c.completion.height = "30%"
c.completion.web_history.max_items = 100000
c.content.cookies.accept = 'no-3rdparty'
c.content.blocking.method = 'both'
c.downloads.location.directory = '/tmp'
c.downloads.location.prompt = False
c.downloads.open_dispatcher = 'rifle'
c.editor.command = [
    'kitty', 'nvim', '{file}', '+normal {line}G{column0}l']
c.fileselect.handler = 'external'
c.fileselect.single_file.command = [
    'kitty', 'ranger', '--clean', '--choosefile={}']
c.fileselect.multiple_files.command = [
    'kitty', 'ranger', '--clean', '--choosefiles={}']

c.hints.auto_follow = 'always'
c.hints.mode = 'number'
c.hints.next_regexes = [rf'\b{word}\b' for word in (
    "next", "older", "more", "continue",
    "→", "≫", "»", "-?>?>",
    "další", "následující", "starší",
)]
c.hints.prev_regexes = [rf'\b{word}\b' for word in (
    "prev(ious)?", "newer", "back",
    "←", "≪", "«", "<<?-?",
    "předchozí", "novější",
)]
c.qt.args = [
    # "ppapi-widevine-path=/usr/lib/chromium/libwidevinecdmadapter.so",
    "ppapi-widevine-path=~/.config/qutebrowser/libwidevinecdmadapter.so",
]
# c.qt.force_software_rendering = 'qt-quick'
# c.qt.process_model = 'process-per-site'
c.session.lazy_restore = True
# /usr/share/qutebrowser/scripts/dictcli.py install \
# cs-CZ en-US de-DE es-ES ru-RU
c.spellcheck.languages = ['en-US']  # , 'en-US', 'de-DE']
c.scrolling.smooth = True
c.tabs.background = True
c.url.auto_search = 'never'

c.url.searchengines = {
    'DEFAULT': 'https://duckduckgo.com/?q={}',
    'd': 'https://duckduckgo.com/?q={}',
    'ddg': 'https://duckduckgo.com/?q={}',
    'g': 'https://www.google.com/search?hl=en&q={}',
    'w': 'https://cs.wikipedia.org/w/index.php?search={}',
    'we': 'https://en.wikipedia.org/w/index.php?search={}',
    'en': 'https://slovnik.seznam.cz/preklad/cesky_anglicky/{}',
    'de': 'https://slovnik.seznam.cz/preklad/cesky_nemecky/{}',
    'es': 'https://slovnik.seznam.cz/preklad/cesky_spanelsky/{}',
    'fr': 'https://slovnik.seznam.cz/preklad/cesky_francouzsky/{}',
    'ru': 'https://slovnik.seznam.cz/preklad/cesky_rusky/{}',
    'debian': 'https://packages.debian.org/search?keywords={}',
    'ubuntu': 'https://packages.ubuntu.com/search?keywords={}',
    'uloz': 'https://uloz.to/hledej?orderby=largest&q={}',
    # 'kol': utils.get_coworkers_addr(),
    # 'kol': 'http://intranet/search/Stranky/PeopleResults.aspx?k={}',
    'netflix': 'https://www.netflix.com/search?q={}',
    'pypi': 'https://pypi.org/search/?q={}',
}

c.window.hide_decoration = True

# base16.set_colors(c, base16.GRUVBOX)

config.load_autoconfig(False)
config.set('content.headers.user_agent', (
               'Mozilla/5.0 (X11; Linux x86_64) '
               'AppleWebKit/537.36 (KHTML, like Gecko) '
               'Chrome/111.0.0.0 Safari/537.36'
), 'accounts.google.com')

# PER DOMAIN
with config.pattern('http://localhost:3000') as p:
    p.content.cookies.accept = 'all'

with config.pattern('https://www.google.com') as p:
    p.content.geolocation = True

with config.pattern('https://mail.google.com') as p:
    p.content.notifications.enabled = True

with config.pattern('https://web.skype.com') as p:
    p.content.notifications.enabled = True

with config.pattern('https://outlook.office.com/*') as p:
    p.content.register_protocol_handler = True

with config.pattern('https://www.webrtc-experiment.com') as p:
    p.content.desktop_capture = True

with config.pattern('https://mozilla.github.io') as p:
    p.content.desktop_capture = True

with config.pattern('https://teams.microsoft.com/*') as p:
    p.content.notifications.enabled = True
    p.content.cookies.accept = 'all'
    p.content.media.audio_video_capture = True
    p.content.desktop_capture = True

with config.pattern('https://web.facebook.com') as p:
    p.content.notifications.enabled = True

with config.pattern('https://web.whatsapp.com') as p:
    p.content.notifications.enabled = True

with config.pattern('https://www.aliexpress.com') as p:
    p.content.notifications.enabled = True

with config.pattern('https://www.reddit.com') as p:
    p.content.notifications.enabled = True

with config.pattern('https://mozilla.github.io') as p:
    c.content.media.audio_video_capture = True
    p.content.desktop_capture = True

with config.pattern('https://www.celiostore.cz') as p:
    p.content.geolocation = True

with config.pattern('https://www.zlatezrnko.cz') as p:
    p.content.geolocation = True

with config.pattern('https://www.zasilkovna.cz') as p:
    p.content.geolocation = True

# BINDINGS
# unused: ~!#%^&*()_    e[]a\zxc,    QWETYUI{}ZXCVBMN<>
# REPLACE `:` WITH `;`
config.unbind(':')
for key, command in c.bindings.default['normal'].items():
    if key.startswith(';'):
        config.bind(key.replace(';', ':', 1), command)
        config.bind(key.replace(';', ']', 1), command)
config.bind(';', 'cmd-set-text :')

# MOVING AROUND
scroll_steps, rest_px = utils.get_scroll_vars()
config.bind('<Backspace>', (f'scroll-px 0 {-rest_px} ;;'
                            f'cmd-run-with-count {scroll_steps} scroll up'))
config.bind('<Space>', (f'scroll-px 0 {rest_px} ;;'
                        f'cmd-run-with-count {scroll_steps} scroll down'))

# TABS
config.bind('t', 'cmd-set-text -s :open -t')
# config.bind('b', 'cmd-set-text -rs :buffer')
config.bind('b', 'cmd-set-text -rs :tab-select')
config.bind('<Ctrl-J>', 'tab-next')
config.bind('<Ctrl-K>', 'tab-prev')
config.bind('ó', 'tab-prev')
config.bind('π', 'tab-next')
for i, altkey in enumerate('█²³⁴½', start=1):
    config.bind(f'<Ctrl+{i}>', f'tab-select {i}')
    config.bind(altkey, f'tab-select {i}')
config.bind('<Ctrl-E>', 'tab-focus -1')
config.bind('g<Ctrl-E>', 'tab-move -1')
config.bind('<Ctrl-Escape>', 'tab-focus last')

# SEARCHING
config.bind('s', 'cmd-set-text -s :open g')
config.bind('S', 'cmd-set-text :google \'')
# config.bind('S', 'cmd-set-text -s :open -t g')
config.bind('w', 'cmd-set-text :wikipedia \'')
# config.bind('w', 'cmd-set-text -s :open -t we')

# UI
config.unbind('<Ctrl-H>')   # homepage
config.bind('xx', ('config-cycle statusbar.show never always ;;'
                   'config-cycle tabs.show switching always'))
config.bind('xb', 'config-cycle statusbar.show never always')
config.bind('xt', 'config-cycle tabs.show switching always')
config.bind('xm', 'tab-mute')
config.unbind('M')
config.bind('<Shift-Escape>', 'fake-key <Escape>')  # S-Esc sends Esc directly
config.bind('yl', 'hint --rapid links yank')
config.bind('xc', 'spell-cs')
config.bind('xe', 'spell-en')

# DEV
config.bind('I', 'devtools right')
config.bind('gI', 'devtools bottom')

# EXTERNAL
config.bind('ř', 'spawn mpv {url}')
config.bind(':ř', 'hint links spawn mpv {hint-url}')
config.bind(']ř', 'hint links spawn mpv {hint-url}')
config.bind(']Ř', 'hint links --rapid spawn mpv {hint-url}')
config.bind('→', 'spawn --userscript password_fill')
config.bind('↓', 'spawn sh -c "echo {url} >> /tmp/links"')
config.bind(']↓', 'hint links spawn sh -c "echo {hint-url} >> /tmp/links"')
config.bind(']↑', 'hint --rapid links spawn sh -c "echo {hint-url} >> /tmp/links"')
config.bind('<Ctrl-I>', 'cmd-edit', mode='command')
config.bind('<Ctrl-I>', 'edit-text', mode='insert')
config.bind('<Ctrl-I>', 'edit-url', mode='normal')
config.bind('<Insert>', 'cmd-edit', mode='command')
config.bind('<Insert>', 'edit-text', mode='insert')
config.bind('<Insert>', 'edit-url', mode='normal')

# DOWNLOADING
config.bind('ď', 'download-open ;; download-remove')
config.bind('Ď', 'download-open dragon --and-exit ;; download-remove')

# GAMEPAD BINDINGS
config.bind('<Ctrl-Left>', 'back')
config.bind('<Ctrl-Right>', 'forward')
config.bind('<Ctrl-Up>', 'tab-close')
config.bind('<Ctrl-Down>', 'cmd-set-text -s :open -t')
config.bind('<Ctrl-Alt-Left>', 'tab-prev')
config.bind('<Ctrl-Alt-Right>', 'tab-next')
config.bind('<Ctrl-Alt-Up>', 'navigate prev')
config.bind('<Ctrl-Alt-Down>', 'navigate next')

# EMACS IN INSERT MODE
config.bind('<Ctrl-B>', 'fake-key <Left>', mode='insert')
config.bind('<Ctrl-F>', 'fake-key <Right>', mode='insert')
config.bind('<Ctrl-A>', 'fake-key <Home>', mode='insert')
config.bind('<Ctrl-E>', 'fake-key <End>', mode='insert')
config.bind('<Ctrl-N>', 'fake-key <Down>', mode='insert')
config.bind('<Ctrl-P>', 'fake-key <Up>', mode='insert')
config.bind('<Ctrl-H>', 'fake-key <Backspace>', mode='insert')
config.bind('<Ctrl-D>', 'fake-key <Delete>', mode='insert')
config.bind('<Ctrl-W>', 'fake-key <Ctrl-Backspace>', mode='insert')
config.bind('<Ctrl-U>',
            'fake-key <Shift-Home> ;; fake-key <Delete>', mode='insert')
config.bind('<Ctrl-K>',
            'fake-key <Shift-End> ;; fake-key <Delete>', mode='insert')
