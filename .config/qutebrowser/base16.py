# base16-qutebrowser (https://github.com/theova/base16-qutebrowser)
# Base16 qutebrowser template by theova
# Gruvbox dark, medium scheme by Dawid Kurek (dawikur@gmail.com), morhetz
# (https://github.com/morhetz/gruvbox)


GRUVBOX = [
    "#282828",  # 0x00
    "#3c3836",  # 0x01
    "#504945",  # 0x02
    "#665c54",  # 0x03
    "#bdae93",  # 0x04
    "#d5c4a1",  # 0x05
    "#ebdbb2",  # 0x06
    "#fbf1c7",  # 0x07
    "#fb4934",  # 0x08
    "#fe8019",  # 0x09
    "#fabd2f",  # 0x0A
    "#b8bb26",  # 0x0B
    "#8ec07c",  # 0x0C
    "#83a598",  # 0x0D
    "#d3869b",  # 0x0E
    "#d65d0e",  # 0x0F
]


def set_colors(c, colors):
    # set qutebrowser colors
    c.colors.completion.category.bg = colors[0x00]
    c.colors.completion.category.border.bottom = colors[0x00]
    c.colors.completion.category.border.top = colors[0x00]
    c.colors.completion.category.fg = colors[0x0A]
    c.colors.completion.fg = colors[0x05]
    c.colors.completion.item.selected.bg = colors[0x0A]
    c.colors.completion.item.selected.border.bottom = colors[0x0A]
    c.colors.completion.item.selected.border.top = colors[0x0A]
    c.colors.completion.item.selected.fg = colors[0x01]
    c.colors.completion.match.fg = colors[0x0B]
    c.colors.completion.odd.bg = colors[0x03]
    c.colors.completion.even.bg = colors[0x00]
    c.colors.completion.scrollbar.bg = colors[0x00]
    c.colors.completion.scrollbar.fg = colors[0x05]
    c.colors.downloads.bar.bg = colors[0x00]
    c.colors.downloads.error.fg = colors[0x08]
    c.colors.downloads.start.bg = colors[0x0D]
    c.colors.downloads.start.fg = colors[0x00]
    c.colors.downloads.stop.bg = colors[0x0C]
    c.colors.downloads.stop.fg = colors[0x00]
    c.colors.hints.bg = colors[0x0A]
    c.colors.hints.fg = colors[0x00]
    c.colors.hints.match.fg = colors[0x05]
    c.colors.keyhint.bg = colors[0x00]
    c.colors.keyhint.fg = colors[0x05]
    c.colors.keyhint.suffix.fg = colors[0x05]
    c.colors.messages.error.fg = colors[0x00]
    c.colors.messages.error.bg = colors[0x08]
    c.colors.messages.error.border = colors[0x08]
    c.colors.messages.info.bg = colors[0x00]
    c.colors.messages.info.border = colors[0x00]
    c.colors.messages.info.fg = colors[0x05]
    c.colors.messages.warning.bg = colors[0x0E]
    c.colors.messages.warning.border = colors[0x0E]
    c.colors.messages.warning.fg = colors[0x00]
    c.colors.prompts.bg = colors[0x00]
    c.colors.prompts.border = colors[0x00]
    c.colors.prompts.fg = colors[0x05]
    c.colors.prompts.selected.bg = colors[0x0A]
    c.colors.statusbar.caret.bg = colors[0x0E]
    c.colors.statusbar.caret.fg = colors[0x00]
    c.colors.statusbar.caret.selection.bg = colors[0x0D]
    c.colors.statusbar.caret.selection.fg = colors[0x00]
    c.colors.statusbar.command.bg = colors[0x00]
    c.colors.statusbar.command.fg = colors[0x05]
    c.colors.statusbar.command.private.bg = colors[0x00]
    c.colors.statusbar.command.private.fg = colors[0x05]
    c.colors.statusbar.insert.bg = colors[0x0D]
    c.colors.statusbar.insert.fg = colors[0x00]
    c.colors.statusbar.normal.bg = colors[0x00]
    c.colors.statusbar.normal.fg = colors[0x0B]
    c.colors.statusbar.passthrough.bg = colors[0x0C]
    c.colors.statusbar.passthrough.fg = colors[0x00]
    c.colors.statusbar.private.bg = colors[0x03]
    c.colors.statusbar.private.fg = colors[0x00]
    c.colors.statusbar.progress.bg = colors[0x0D]
    c.colors.statusbar.url.error.fg = colors[0x08]
    c.colors.statusbar.url.fg = colors[0x05]
    c.colors.statusbar.url.hover.fg = colors[0x05]
    c.colors.statusbar.url.success.http.fg = colors[0x0C]
    c.colors.statusbar.url.success.https.fg = colors[0x0B]
    c.colors.statusbar.url.warn.fg = colors[0x0E]
    c.colors.tabs.bar.bg = colors[0x00]
    c.colors.tabs.even.bg = colors[0x00]
    c.colors.tabs.even.fg = colors[0x05]
    c.colors.tabs.indicator.error = colors[0x08]
    c.colors.tabs.indicator.start = colors[0x0D]
    c.colors.tabs.indicator.stop = colors[0x0C]
    c.colors.tabs.odd.bg = colors[0x03]
    c.colors.tabs.odd.fg = colors[0x05]
    c.colors.tabs.selected.even.bg = colors[0x05]
    c.colors.tabs.selected.even.fg = colors[0x00]
    c.colors.tabs.selected.odd.bg = colors[0x05]
    c.colors.tabs.selected.odd.fg = colors[0x00]
    # c.colors.webpage.bg = colors[0x00]
