import html
import json
import requests
try:
    from PyQt6.QtCore import QUrl
except ModuleNotFoundError:
    from PyQt5.QtCore import QUrl

import qutebrowser
from qutebrowser.api import cmdutils
from qutebrowser.completion.models import completionmodel

from .base import BaseCategory


class SlovnikCategory(BaseCategory):
    name = 'Seznam Slovník Suggestions'

    def __init__(self, lang, **kwargs):
        super(SlovnikCategory, self).__init__(**kwargs)
        self.lang = lang

    def get_suggestions(self, val):
        response = requests.get(
            url=f'https://slovnik.seznam.cz/suggest/mix_cz_{self.lang}',
            params={'phrase': val, 'result': 'json'}
            # 'highlight': '1', 'count': '6',
        )
        if response.status_code != 200:
            return
        try:
            data = json.loads(response.content)
            data['result'][0]['suggest']
        except Exception:
            return

        for i in data['result'][0]['suggest']:
            yield i['value'],


def get_slovnik_model(lang):
    def slovnik_model(*, info):
        model = completionmodel.CompletionModel(column_widths=(100,))
        model.add_category(SlovnikCategory(lang=lang))
        return model
    return slovnik_model


# def en_model(*, info):
#     model = completionmodel.CompletionModel(column_widths=(100,))
#     model.add_category(SlovnikCategory(lang=lang))
#     return model


def register_command(lang):
    @cmdutils.register(name=lang)
    @cmdutils.argument('query', completion=get_slovnik_model(lang=lang))
    def slovnik(*query):
        query_str = html.escape(' '.join(query))
        qutebrowser.app.open_url(
            url=QUrl(f'https://slovnik.seznam.cz/{lang}-cz?q={query_str}')
        )

    slovnik.__doc__ = f"""Translate {lang.upper()}-EN"""


@cmdutils.register(name='en')
@cmdutils.argument('query', completion=get_slovnik_model(lang='en'))
def slovnik_en(*query):
    """Translate CS-EN"""
    query_str = html.escape(' '.join(query))
    qutebrowser.app.open_url(
        url=QUrl(f'https://slovnik.seznam.cz/en-cz?q={query_str}')
    )


@cmdutils.register(name='de')
@cmdutils.argument('query', completion=get_slovnik_model(lang='de'))
def slovnik_de(*query):
    """Translate CS-DE"""
    query_str = html.escape(' '.join(query))
    qutebrowser.app.open_url(
        url=QUrl(f'https://slovnik.seznam.cz/de-cz?q={query_str}')
    )


@cmdutils.register(name='es')
@cmdutils.argument('query', completion=get_slovnik_model(lang='es'))
def slovnik_es(*query):
    """Translate CS-ES"""
    query_str = html.unescape(' '.join(query))
    qutebrowser.app.open_url(
        url=QUrl(f'https://slovnik.seznam.cz/es-cz?q={query_str}')
    )
