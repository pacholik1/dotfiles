# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
		# shellcheck disable=SC1091
		. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/go/bin" ] ; then
    PATH="$HOME/go/bin:$PATH"
	export PATH
fi
if [ -d "$HOME/node_modules/.bin" ] ; then
    PATH="$HOME/node_modules/.bin:$PATH"
	export PATH
fi
if [ -n "$CARGO_HOME" ] && [ -d "$CARGO_HOME/bin" ] ; then
        export PATH="$CARGO_HOME/bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
	export PATH
fi
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
	export PATH
fi

#moje proměnné
# export LANG=C.UTF-8
export TERM=xterm-kitty
export TERMCMD=kittyc
export PAGER=vimpager
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export QT_LOGGING_RULES="qt5ct.debug=false"
export QT_QPA_PLATFORM='wayland;xcb'
export QT_QPA_PLATFORMTHEME=qt5ct
export XDG_SESSION_TYPE=wayland
# export XDG_CURRENT_DESKTOP=Unity
export XDG_CURRENT_DESKTOP=sway
# export WAYLAND_DISPLAY=wayland-1
export MOZ_ENABLE_WAYLAND=1
export _JAVA_AWT_WM_NONREPARENTING=1
export FZF_DEFAULT_COMMAND='find . \('\
'       -name node_modules           '\
'    -o -name .git                   '\
'    -o -name dist                   '\
'    -o -name build                  '\
'    -o -name __pycache__            '\
' \) -prune -o -printf %P\\n         '
# export GOPATH="$HOME/.go"
# export CARGO_HOME="$HOME/.cargo"
# export NODE_PATH="$HOME/.node_modules"
# export NODE_OPTIONS=--openssl-legacy-provider
export EASYRSA_CERT_EXPIRE=3650
export QUTE_QT_WRAPPER=PyQt6
export OPENAI_API_KEY=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
export DEEPL_AUTH_KEY=17e924b4-b43f-48d3-a332-96d275ad5474:fx
