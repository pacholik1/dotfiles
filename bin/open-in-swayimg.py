#!/usr/bin/python3

# Copyright (C) 2022  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.


###
# Open pics in arg’s folder, with arg as the first.
###

import sys
import pathlib
import subprocess
import functools


def _cast_args(f):
    @functools.wraps(f)
    def wrapped(*args):
        typed_args = (tpe(val) for tpe, val in
                      zip(f.__annotations__.values(), args))
        return f(*typed_args)
    return wrapped


@_cast_args
def main(pic: pathlib.Path):
    pics = list(pic.parent.iterdir())
    pics.sort(key=lambda p: p.name.lower())
    idx = pics.index(pic)
    shifted = pics[idx:] + pics[:idx]
    subprocess.run(['swayimg'] + shifted)


if __name__ == '__main__':
    main(*sys.argv[1:])
