#!/bin/sh -eu

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# --servercert='sha256:fafc7521162e89231eea0c1eb54dfcb87c1a1b26a5119c0e68ad5e79f39126de'

passcred work/mel/mikro.mikroelektronika.cz | (
	read -r user
	sudo openconnect \
		--user="$user" \
		--passwd-on-stdin \
		--certificate="$HOME/work/mel/vpn/mycert.pem" \
		--sslkey="$HOME/work/mel/vpn/mykey.pem" \
		--servercert='pin-sha256:+vx1IRYuiSMe6gwetU38uHwaGyalEZwOaK1eefORJt4=' \
		'fw.mikroelektronika.cz'
)
