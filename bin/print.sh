#!/bin/sh -eu

# Copyright (C) 2023  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Print file on printer over ssh.
#
# deps: ssh, qpdf
###


if [ "$#" -eq 1 ]; then
	exec ssh pi@printer.home lp - < "$1"
else
	qpdf --empty --pages "$@" -- - | exec ssh pi@printer.home lp -
fi

# ssh pi@printer.home lp -o fit-to-page - < "$1"
