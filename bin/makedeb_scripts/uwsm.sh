#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -En "/^[ \t]*version:/s/[^']*'([^']*)'.*/\1/p" meson.build)"
isnewer.sh 'uwsm' "$version"

set -- debhelper dh-python meson
sudo aptitude -y install "$@"
dpkg-buildpackage -b -tc --no-sign

deb=../uwsm_${version}*_all.deb
sudo apt install "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
