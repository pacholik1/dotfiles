#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

# version="$(sed -n '/Release: /s///p' notekit.spec)"
version=1.0
isnewer.sh 'notekit' "$version"
dir="notekit_$version"

set -- cmake libgtkmm-3.0-dev libgtksourceviewmm-3.0-dev libjsoncpp-dev zlib1g-dev libfontconfig1-dev
sudo aptitude -y install "$@"
cmake .
make

mkdir -p "$dir/DEBIAN"

printf 'Package: notekit
Version: %s
Section: net
Priority: optional
Architecture: amd64
Depends: libgtkmm-3.0-1v5, libgtksourceviewmm-3.0-0v5, libjsoncpp1, zlib1g, libfontconfig1
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: This program is a structured notetaking application based on GTK+ 3. Write your notes in instantly-formatted Markdown, organise them in a tree of folders that can be instantly navigated from within the program, and add hand-drawn notes by mouse, touchscreen or digitiser.

' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin/"
mkdir -p "$dir/usr/share/notekit/"
cp cmake-build-Release/output/notekit "$dir/usr/bin/"
cp -r data/ sourceview/ "$dir/usr/share/notekit/"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
