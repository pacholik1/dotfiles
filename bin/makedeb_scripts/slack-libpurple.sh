#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

# version="$(grep -A11 PurplePluginInfo slack.c | sed -E '$!d;s/\s+"(.*)",/\1/')"
version="$(sed -En '/PurplePluginInfo/{:a;n;0~11!ba;s/\s+"(.*)",/\1/p}' slack.c)"
isnewer.sh 'slack-libpurple' "$version"
dir="slack-libpurple_$version"

set -- libpurple-dev
sudo aptitude -y install "$@"
make

mkdir -p "$dir/DEBIAN"

printf 'Package: slack-libpurple
Version: %s
Section: net
Priority: optional
Architecture: amd64
Depends: libpurple0
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: A Slack protocol plugin for libpurple IM clients.
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/lib/purple-2"
mkdir -p "$dir/usr/share/doc/slack"
mkdir -p "$dir/usr/share/pixmaps/pidgin/protocols/16"
mkdir -p "$dir/usr/share/pixmaps/pidgin/protocols/22"
mkdir -p "$dir/usr/share/pixmaps/pidgin/protocols/48"
cp libslack.so "$dir/usr/lib/purple-2/"
cp COPYING "$dir/usr/share/doc/slack/"
cp README.md "$dir/usr/share/doc/slack/"
cp img/slack16.png "$dir/usr/share/pixmaps/pidgin/protocols/16/slack.png"
cp img/slack22.png "$dir/usr/share/pixmaps/pidgin/protocols/22/slack.png"
cp img/slack48.png "$dir/usr/share/pixmaps/pidgin/protocols/48/slack.png"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
