#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version=$(sed -En '/^version = /{s/[^"]*"([^"]*)".*/\1/p;q}' Cargo.toml)
isnewer.sh 'cargo-deb' "$version"

set -- cargo
sudo aptitude -y install "$@"
cargo deb

deb="target/debian/cargo-deb_${version}_$(dpkg --print-architecture).deb"
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
