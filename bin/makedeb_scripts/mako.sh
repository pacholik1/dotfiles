#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -En "/^[ \t]*version:/s/[^']*'([^']*)'.*/\1/p" meson.build)"
isnewer.sh 'mako' "$version"
dir="mako_$version"

set -- meson pkg-config libcairo2-dev libpango1.0-dev libsystemd-dev libwayland-dev wayland-protocols scdoc libgdk-pixbuf2.0-dev
sudo aptitude -y install "$@"
meson build
ninja -C build

mkdir -p "$dir/DEBIAN"

printf 'Package: mako
Version: %s
Section: x11
Priority: optional
Architecture: amd64
Depends: libcairo2, libpango-1.0-0, libsystemd0, libwayland-bin, wayland-protocols, libgdk-pixbuf2.0-0
Recommends: 
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: A lightweight notification daemon for Wayland. Works on Sway.
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/local/bin"
mkdir -p "$dir/lib/systemd/system"
cp build/mako "$dir/usr/local/bin/mako"
cp makoctl "$dir/usr/local/bin/makoctl"
cp build/mako.service "$dir/lib/systemd/system"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
