#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -En '/^#define VERSION /s/[^"]*"([^"]*)".*/\1/p' dragon.c)"
isnewer.sh 'dragon' "$version"
dir="dragon_$version"

set -- libgtk-3-dev
sudo aptitude -y install "$@"
make

mkdir -p "$dir/DEBIAN"

printf 'Package: dragon
Version: %s
Section: graphics
Priority: optional
Architecture: amd64
Depends: libgtk-3-0
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: dragon - simple drag-and-drop source/sink for X or Wayland
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin"
cp dragon "$dir/usr/bin/dragon"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
