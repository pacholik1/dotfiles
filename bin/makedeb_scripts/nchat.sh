#!/bin/sh -e

megals /Root > /dev/null 2>&1 &

version="$(sed -En '/^#define NCHAT_VERSION /s/[^"]*"([^"]*)".*/\1/p' lib/common/src/version.h)"
isnewer.sh 'nchat' "$version"
dir="nchat_$version"

set -- ccache cmake build-essential gperf help2man libreadline-dev libssl-dev libncurses-dev ncurses-doc zlib1g-dev libsqlite3-dev libmagic-dev golang
sudo aptitude -y install "$@"
./make.sh build

mkdir -p "$dir/DEBIAN"

printf 'Package: nchat
Version: %s
Section: net
Priority: optional
Architecture: amd64
Depends: libssl3, libncurses6, zlib1g, libsqlite3-0, libmagic1
Maintainer: Vojtěch Pachol <pacholick@gmail.com>
Description: nchat is a terminal-based chat client for Linux and macOS with support for Telegram and WhatsApp.
' "$version" > "$dir/DEBIAN/control"

mkdir -p "$dir/usr/bin"
mkdir -p "$dir/usr/lib"
cp build/bin/nchat "$dir/usr/bin/nchat"
cp build/lib/libduchat.so "$dir/usr/lib"
cp build/lib/libncutil.so "$dir/usr/lib"
cp build/lib/libtdclientshared.so "$dir/usr/lib"
cp build/lib/libwmchat.so "$dir/usr/lib"
dpkg -b "$dir"

deb=$dir.deb
sudo dpkg -i "$deb"
sudo aptitude -y markauto "$@"
wait
megaput "$deb" --path=/Root/debs
