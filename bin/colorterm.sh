#!/bin/sh -eu

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Open a plain random coloured window.
###

grep -Po '#[0-9a-d][0-9a-f]{5}' ~/.config/kitty/gruvbox_dark.conf | shuf -n1 | (
	read -r color
	kitty --detach --override "background=$color" -- bash -c "tput civis; read -rsn1"
)
