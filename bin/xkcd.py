#!/usr/bin/python3

# Copyright (C) 2018  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

"""Create kindle collection of xkcd strips"""
# deps: mkbc, beautifulsoup4

import os
import sys
import httplib2
from bs4 import BeautifulSoup
import subprocess

import mkbc


def get_article(url: str,
                imgs: bool = False, maths: bool = True) -> mkbc.Article:
    context_element = ["div", {"id": "comic"}]

    h = httplib2.Http(mkbc.CACHE_DIR, disable_ssl_certificate_validation=True)
    try:
        response, content = h.request(url)
    except Exception:
        print(url)
        raise
    # soup = BeautifulSoup(content)
    soup = BeautifulSoup(content, 'lxml')
    title = soup.find("title").contents[0]
    snip = soup.find(*context_element)

    imglist = []
    if imgs:
        for img in snip.findAll("img"):
            # print(img)
            img['src'] = mkbc.images[url, img.get('src', '')]
            imglist.append(img)

    content = """{snip}<p style="page-break-before:always">{alt}</p>""".format(
        snip=str(snip), alt=imglist[0]["title"] if imglist else None)
    return mkbc.Article(title, content, imglist)


def main(*argv):
    r = range(3000, 3051)   # did not read those yet
    articles = [
        get_article("https://xkcd.com/{}".format(i), imgs=True, maths=False)
        for i in r
        # if i not in [2198]
    ]

    filename = mkbc.make_epub(articles,
                             name=f"xkcd_{r.start}-{r.stop - 1}",
                             titles=True)
    subprocess.run(['kepubify', '--inplace', filename])
    kepub = filename.replace('.epub', '.kepub.epub')
    os.chmod(kepub, 0o644)
    print(kepub)


if __name__ == '__main__':
    main(sys.argv[1:])
