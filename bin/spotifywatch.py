#!/usr/bin/python3

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

"""watch for song change"""
# deps: dbus, dbus_next, psutil

import sys
import asyncio
import dbus
import dbus_next
import psutil
import signal
import pathlib
import json
import textwrap

WAYBAR_P = next(p for p in psutil.process_iter(['name']) if
                p.info['name'] == 'waybar')
JSON = pathlib.Path('/tmp/spotifywatch.json')


async def watch(action, *args, **kwargs):
    bus = await dbus_next.aio.MessageBus().connect()
    introspection = await _stubborn(
        bus.introspect,
        'org.mpris.MediaPlayer2.spotify', '/org/mpris/MediaPlayer2')
    obj = bus.get_proxy_object(
        'org.mpris.MediaPlayer2.spotify', '/org/mpris/MediaPlayer2',
        introspection)
    properties = obj.get_interface('org.freedesktop.DBus.Properties')
    player = obj.get_interface('org.mpris.MediaPlayer2.Player')

    metadata = await player.get_metadata()
    action(metadata, *args, **kwargs)

    def on_properties_changed(interface_name, changed_properties,
                              invalidated_properties):
        action(changed_properties['Metadata'].value, *args, **kwargs)

    properties.on_properties_changed(on_properties_changed)


def _dict_from_metadata(metadata, *args, **kwargs):
    return {
        'title': metadata['xesam:title'].value,
        'artists': ", ".join(metadata['xesam:artist'].value),
        'album': metadata['xesam:album'].value,
        'length': "{}:{}".format(*divmod(
            metadata['mpris:length'].value // 1000000, 60)),
        'pic': metadata['mpris:artUrl'].value,
        'url': metadata['xesam:url'].value,
        'trackid': metadata['mpris:trackid'].value,
    }


async def _stubborn(f, *args):
    while True:
        try:
            result = await f(*args)
        except Exception:
            await asyncio.sleep(10)
        else:
            return result


def write_json_and_send_signal(metadata):
    dct = _dict_from_metadata(metadata)

    with JSON.open('w') as f:
        text = f"{dct['artists']} – {dct['title']}"
        text = textwrap.shorten(text, 60)
        json.dump({
            'text': text,
            'tooltip': "\n".join(f"{k}: {v}" for k, v in dct.items())
        }, f)

    WAYBAR_P.send_signal(signal.SIGRTMIN+8)


def _pause():
    bus = dbus.SessionBus()
    proxy = bus.get_object('org.mpris.MediaPlayer2.spotify',
                           '/org/mpris/MediaPlayer2')
    interface = dbus.Interface(proxy,
                               dbus_interface='org.mpris.MediaPlayer2.Player')
    interface.Pause()


def pauseafter(metadata, num=1, history=[]):
    num = int(num)
    trackid = metadata['mpris:trackid'].value

    if not history:
        history.append(trackid)
        return

    if history[-1] != trackid:
        if len(history) >= num:
            _pause()
            sys.exit()
        history.append(trackid)


async def main(command, *args, **kwargs):
    match command:
        case 'waybar':
            action = write_json_and_send_signal
        case 'pauseafter':
            action = pauseafter
        case _:
            sys.exit("Allowed commands are ‘waybar’ and ‘pauseafter’")

    await watch(action, *args, **kwargs)
    await asyncio.get_event_loop().create_future()


if __name__ == '__main__':
    asyncio.run(main(*sys.argv[1:]))
