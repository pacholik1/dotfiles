#!/usr/bin/python3

# Copyright (C) 2018  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

"""Lists all my jira issues"""
# deps: passtodict, jira

import os
import sys
import warnings
import passtodict
from jira import JIRA
import typing


_commands = {}


def command(f: typing.Callable):
    _commands[f.__name__] = f


def get_cred() -> tuple[str, str]:
    cred = passtodict('work/mikro.mikroelektronika.cz')
    username = cred.LOGIN
    password = cred.PWD
    return username, password


@command
def printall():
    _stderr = sys.stderr
    with open(os.devnull, 'w') as null:
        sys.stderr = null
    jira = JIRA(server='https://jira.mikroelektronika.cz',
                basic_auth=get_cred(),
                options={'verify': False})
    sys.stderr = _stderr

    issues = jira.search_issues(
        'project=ASW AND '
        'sprint IN openSprints() AND '
        'assignee=currentUser() '
        'ORDER BY key',
        maxResults=1000)

    for i in issues:
        print(f'{i.key} – {i.fields.summary}')


@command
def help():
    print("commands:")
    for command in _commands:
        print(f'  {command}')


def main(command: str):
    _commands.get(command, help)()


if __name__ == '__main__':
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        main(*sys.argv[1:])
