#!/usr/bin/python3

# Copyright (C) 2024  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import sys
import subprocess as s
from dataclasses import dataclass
from tempfile import mkstemp
from pathlib import Path


@dataclass
class Geometry:
    X: float
    Y: float
    W: float
    H: float

    def __truediv__(self, other: 'Geometry'):
        x = (self.X - other.X) / other.W
        y = (self.Y - other.Y) / other.H
        w = self.W / other.W
        h = self.H / other.H
        return Geometry(x, y, w, h)

    def to_geom(self):
        return f'{self.X},{self.Y} {self.W}x{self.H}'

    def to_css(self):
        return f'''{{
    left: {self.X:.1%};
    top: {self.Y:.1%};
    width: {self.W:.1%};
    height: {self.H:.1%};
}}'''

    def ocr(self, *args):
        _, pathname = mkstemp(suffix='.png', dir='/tmp/')
        path = Path(pathname)

        s.check_call(['grim', '-g', self.to_geom(), pathname])
        tesseract = s.Popen(['tesseract', *args, pathname, '-'], stdout=s.PIPE, stderr=s.DEVNULL)
        vipe_out = s.check_output(['vipe'], stdin=tesseract.stdout)
        path.unlink()
        return vipe_out.decode().strip()


def slurp():
    slurp_out = s.check_output(['slurp', '-f%x %y %w %h'])
    return Geometry(*[int(i) for i in slurp_out.decode('utf-8').split()])


def main(*args):
    # geom0 = Geometry(17, 124, 1188, 830)     # geometry of the viewport
    geom0 = Geometry(17, 92, 1188, 891)     # geometry of the viewport
    geom = slurp()
    output = '\n'.join((
        geom.ocr(*args),
        (geom / geom0).to_css(),
    ))
    s.run(['wl-copy'], input=output, encoding='utf-8')
    print(output)


if __name__ == '__main__':
    main(*sys.argv[1:])
