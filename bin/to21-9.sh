#!/bin/sh -eu

# Copyright (C) 2019  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Crop a video to 21:9
#
# deps: ffmpeg, coreutils
###

mkdir -p cropped

for i in "$@"; do
	[ -e "$i" ] || continue
	# ffmpeg -i "$i" -vf "crop=ih*21/9:ih" -c:a copy -c:s copy "cropped/${i##*/}"
	ffmpeg -i "$i" -vf "crop=1920:820" -c:a copy -c:s copy "cropped/${i##*/}"
done
