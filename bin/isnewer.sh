#!/bin/sh -eu

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Check if the package is newer than already installed
###

package="$1"
pkgver="$2"

instver=$(apt-cache policy "$package" | sed -n '/  Installed: /s///p')
printf 'package: %s\ninstalled: %s\n' "$pkgver" "$instver"

[ "$instver" = '(none)' ] && exit 0
highver=$(printf '%s\n' "$instver" "$pkgver" | sort -rV | sed '1q')

[ "$instver" != "$highver" ]
