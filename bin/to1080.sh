#!/bin/sh -eu

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Convert a video to 1080 AVC, audio and subtitles being preserved.
#
# deps: ffmpeg, coreutils
###

mkdir -p 1080

for i in "$@"; do
	[ -e "$i" ] || continue
	target="1080/${i##*/}"
	ffmpeg -i "$i" -s hd1080 -c:a copy -c:s copy "$target" || rm "$target"
done
