#!/bin/bash -eu

# Copyright (C) 2017  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Simple stopwatch.
#
# deps: sleepenh, bc, coreutils, ncurses-bin
###

_start=$(sleepenh 0 || true)
_end=''
fgpid=$$

tput civis			# hides cursor
_exit() {
	printf '\r\033[K'	# clear line
	tput cnorm

	_end=$(sleepenh 0 || true)
	seconds=$(echo "$_end" - "$_start" | bc)
	printf '%s seconds\n' "$seconds"
	exit "$1"
}
trap '_exit 130' INT
trap '_exit 143' TERM
trap '_exit 0' RTMAX

onbackground() {
	"$@"
	kill -RTMAX $fgpid
}

if [ "$#" -ne 0 ]; then
	onbackground "$@" &
fi

c=0
while :; do
	printf '\r\033[K'	# clear line

	days=$((c/86400))
	hours=$((c/3600%24))
	minutes=$((c/60%60))
	seconds=$((c%60))

	if [ $c -ge 86400 ]; then
		printf '%dd %02d:%02d:%02d' $days $hours $minutes $seconds
	elif [ $c -ge 3600 ]; then
		printf '%d:%02d:%02d' $hours $minutes $seconds
	elif [ $c -ge 60 ]; then
		printf '%d:%02d' $minutes $seconds
	else
		printf '%d' $seconds
	fi
	c=$((c+1))
	sleepenh "$_start" "$c" >/dev/null &
	wait $!
done

_exit 0
