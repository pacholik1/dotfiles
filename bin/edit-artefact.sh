#!/bin/sh -eu

tgz="$1"
dir="${tgz%.tgz}"

rm -r "$dir"
mkdir "$dir"
cd "$dir"
tar -xvf ../"$tgz"
mv ../"$tgz" ../"$tgz"_

rm appdirs*.whl attrs*.whl Babel*.whl cached_property*.whl certifi*.whl
rm chardet*.whl defusedxml*.whl et_xmlfile*.whl idna*.whl isodate*.whl
rm jdcal*.whl lxml*.whl motor*.whl openpyxl*.whl pycryptodomex*.whl PyJWT*.whl
rm pymongo*.whl python_dateutil*.whl pytz*.whl PyYAML*.whl requests*.whl
rm schematics*.whl six*.whl tornado*.whl urllib3*.whl zeep*.whl

scp -T 192.168.122.249:"                                                    \
appdirs*.whl attrs*.whl Babel*.whl cached_property*.whl certifi*.whl        \
chardet*.whl defusedxml*.whl et_xmlfile*.whl idna*.whl isodate*.whl         \
jdcal*.whl lxml*.whl motor*.whl openpyxl*.whl pycryptodomex*.whl PyJWT*.whl \
pymongo*.whl python_dateutil*.whl pytz*.whl PyYAML*.whl requests*.whl       \
schematics*.whl six*.whl tornado*.whl urllib3*.whl zeep*.whl                \
" .

tar -czf ../"$tgz" -- *
