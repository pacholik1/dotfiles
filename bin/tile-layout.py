#!/usr/bin/python3

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

"""Place windows in Sway like in Awesome"""
# deps: i3ipc, xdg

import asyncio
import logging
import pathlib
import socket
import sys

import i3ipc
import i3ipc.aio
import xdg

logging.basicConfig(filename='/tmp/tile-layout.log', filemode='w')


async def get_workspace(i3: i3ipc.aio.Connection):
    tree = await i3.get_tree()
    focused = tree.find_focused()
    workspace = focused.workspace()     # TODO: NoneType
    return workspace


async def move_right(i3: i3ipc.aio.Connection, container: i3ipc.aio.Con):
    while True:
        workspace = await get_workspace(i3)
        if not workspace.nodes:
            continue
        rightmost = workspace.nodes[-1]
        if not rightmost.find_by_id(container.id):
            await container.command('move right')
        else:
            break


async def on_new_window(i3: i3ipc.aio.Connection, e: i3ipc.events.WindowEvent):
    try:
        workspace = await get_workspace(i3)
        num_nodes = len(workspace.nodes)
        # if num_nodes == 0:
        #     return
        # if num_nodes == 1:
        #     if e.container.layout != 'splith':
        #         await e.container.command('split horizontal')
        if num_nodes > 1:
            right = workspace.nodes[-1]
            if right.layout != 'splitv':
                await right.command('split vertical')
        if num_nodes > 2:
            await move_right(i3, e.container)
    except Exception as ex:
        logging.exception(ex)


async def on_workspace_change(i3: i3ipc.aio.Connection,
                              e: i3ipc.events.WorkspaceEvent):
    if e.old.num != 3 and e.current.num == 3:
        await i3.command('mode byobu')
    if e.old.num == 3 and e.current.num != 3:
        await i3.command('mode default')


async def main():
    hostname = socket.gethostname()

    i3 = await i3ipc.aio.Connection().connect()
    i3.on(i3ipc.Event.WINDOW_NEW, on_new_window)

    swaycfgdir = pathlib.Path(xdg.BaseDirectory.xdg_config_home) / 'sway'
    devices_that_connect_to_rpi = [
        cfg.stem for cfg in swaycfgdir.iterdir() if 'ssh rpi.home' in cfg.read_text()]
    if hostname in devices_that_connect_to_rpi:
        i3.on(i3ipc.Event.WORKSPACE_FOCUS, on_workspace_change)

    await i3.main()


if __name__ == '__main__':
    asyncio.run(main(*sys.argv[1:]))
