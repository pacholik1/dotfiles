#!/usr/bin/python3

# Copyright (C) 2016  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

"""Convert md to html"""
# deps: Markdown

import os
import sys
from markdown import markdown
import webbrowser
import typing


def main(mdfilename: str, htmlfilename: typing.Union[str, None] = None):
    title, ext = os.path.splitext(mdfilename)
    if not htmlfilename:
        # htmlfilename = re.sub(r".md$", r".html", mdfilename)
        htmlfilename = title + '.html'

    with open(mdfilename, mode='r', encoding="UTF-8") as f:
        md = f.read()
    # body = markdown(escape(md))
    body = markdown(md, extensions=['tables'])

    html = f"""<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{title}</title>
    </head>
    <body>
        {body}
    </body>
</html>"""

    with open(htmlfilename, mode='w', encoding="UTF-8") as f:
        f.write(html)

    webbrowser.open(os.path.abspath(htmlfilename))
    # htmldoc --webpage --charset utf-8 --header . --footer . -f osel.ps osel.html


if __name__ == '__main__':
    main(*sys.argv[1:])
