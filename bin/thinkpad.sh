#!/bin/sh -eu

# Copyright (C) 2018  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Do something on thinkpad
#
# debs: ssh, coreutils, hostname
###

file="$(readlink -f "$@" | sed 's # %23 g;s \$ %24 g')"
rpiip=$(hostname --all-ip-addresses | cut -d' ' -f1)
url="http://$rpiip${file#/mnt}"
# hostip=$(who | sed -En '\ pts/0 s .*\((.*)\) \1 p')
hostip=$(w --no-header --no-current | sed -En '\#sshd-session#s#[^0-9.]+([0-9.]+).*#\1#p')

case $(basename "$0") in
	thinkpad.play.sh)
		command='mpv --really-quiet "'"$url"'"'
		;;
	thinkpad.rifle.sh)
		command='rifle "'"$url"'"'
		;;
	thinkpad.copyurl.sh)
		# command='(printf '"'%s'"' "'"$url"'" | xclip -selection clipboard)'
		command='(printf '"'%s'"' "'"$url"'" | wl-copy)'
		;;
	thinkpad.wget.sh)
		command='wget --background --quiet --directory-prefix=/tmp "'"$url"'"'
		;;
	*)
		command='rifle "'"$url"'"'
		;;
esac

exec ssh "$hostip" '
export DISPLAY=:0
export WAYLAND_DISPLAY=wayland-1
'"$command"' </dev/null >/dev/null 2>/dev/null &
'
