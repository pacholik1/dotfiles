#!/bin/sh -e

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Record a video in sway.
#
# deps: slurp, wf-recorder, shotcut, sway, inotify-tools
###

exec 2>/tmp/recordvideo.log

trap '' INT

cd /tmp

file="$(date +%Y%m%d_%H%M%S.mkv)"
if [ -e "$file" ]; then
	file="$(date +%Y%m%d_%H%M%S_%N.mkv)"
fi
echo "$file"

case "$1" in
	-h|--help)
		printf '%s\n' "Take screenshot" \
			"Usage: screenshot.sh" \
			"  or:  screenshot.sh COMMAND" \
			"" \
			"where COMMAND may be:" \
			"    active:  currently active window" \
			"    region:  select region of the screen"
		exit 0
		;;
	active)
		wf-recorder -a -g"$(swaymsg -t get_tree | jq -r "\
				recurse(.nodes[]?, .floating_nodes[]?) | \
				select(.focused) | \
				.rect | \
				\"\(.x),\(.y) \(.width)x\(.height)\" \
				")" -f "$file"
		;;
	region)
		wf-recorder -a -g"$(swaymsg -t get_tree | jq -r "\
				.. | \
				select(.pid? and .visible?) | \
				.rect | \
				\"\(.x),\(.y) \(.width)x\(.height)\" \
				" | slurp -o)" -f "$file"
		;;
	*)
		wf-recorder -a -f "$file"
		;;
esac

echo "$file"
# shotcut "$file" &
# inotifywait --event open "$file"
# rm "$file"
