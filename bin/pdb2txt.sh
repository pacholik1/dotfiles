#!/bin/sh -eu

# Copyright (C) 2017  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Convert WIN1250 coded pdb to UTF8 coded txt
#
# deps: txt2pdbdoc, libc-bin
###

pdb=$1
txt=${2-${1%.*}.txt}

txt2pdbdoc -dD "$pdb" /dev/stdout | iconv -f WINDOWS-1250 -t UTF-8 > "$txt"
