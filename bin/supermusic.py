#!/usr/bin/python3

# Copyright (C) 2018  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

"""Create supermusic songbook for kindle"""
# deps: mkc, beautifulsoup4

import os
from datetime import datetime
from bs4 import BeautifulSoup

import mkc


def make_multiple_page(articles: list[mkc.Article],
                       name: str = "supermusic", titles: bool = False) -> str:
    filename = os.path.join(mkc.CACHE_DIR, name + ".html")
    with open(filename, "w") as f:
        f.write(
            """<!DOCTYPE HTML PUBLIC
            "-//W3C//DTD HTML 4.0//EN"
            "http://www.w3.org/TR/1998/REC-html40-19980424/strict.dtd">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>""")
        f.write(name)
        f.write("""</title>
                </head>
                <body>
                <font size=1>
                """)

        # TOC
        for i in articles:
            f.write("""<a href="#{title}">{title}<br/></a>
                    """.format(title=i.title))

        # content
        for i in articles:
            f.write("""
                    <a style="page-break-before:always" name="{title}"></a>
                    """.format(title=i.title))
            f.write(i.content)

        f.write("</body></html>")

    return filename


def main():
    with open("/home/pacholik/scores/supermusic.html") as f:
        soup = BeautifulSoup(f, 'lxml')
    articles = [
        mkc.get_article(
            "http://www.supermusic.cz/piesen_tlac.php{}".format(e.get('href')),
            imgs=False, maths=False)
        for e in soup.findAll('a')
    ]

    page = make_multiple_page(
        articles, name=datetime.now().strftime('supermusic_%Y-%m-%d'))
    print(mkc.html_to_mobi(page))


if __name__ == '__main__':
    main()
