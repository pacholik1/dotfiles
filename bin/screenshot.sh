#!/bin/sh -e

# Copyright (C) 2020  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Take a screenshot in sway.
#
# deps: grim, slurp, kolourpaint, sway, inotify-tools
###

exec 2>/tmp/screenshot.log

cd /tmp

file="$(date +%Y%m%d_%H%M%S.png)"
if [ -e "$file" ]; then
	file="$(date +%Y%m%d_%H%M%S_%N.png)"
fi
echo "$file"

case "$1" in
	-h|--help)
		printf '%s\n' "Take screenshot" \
			"Usage: screenshot.sh" \
			"  or:  screenshot.sh COMMAND" \
			"" \
			"where COMMAND may be:" \
			"    active:  currently active window" \
			"    region:  select region of the screen"
		exit 0
		;;
	active)
		grim -g"$(swaymsg -t get_tree | jq -r "\
				recurse(.nodes[]?, .floating_nodes[]?) | \
				select(.focused) | \
				.rect | \
				\"\(.x),\(.y) \(.width)x\(.height)\" \
				")" "$file"
		;;
	region)
		grim -g"$(swaymsg -t get_tree | jq -r "\
				.. | \
				select(.pid? and .visible?) | \
				.rect | \
				\"\(.x),\(.y) \(.width)x\(.height)\" \
				" | slurp -o)" "$file"
		;;
	*)
		grim "$file"
		;;
esac

echo "$file"
# kitty +kitten icat "$file"

drawing "$file" &
inotifywait --event open "$file"
sleep .5
rm "$file"
wait
dragon --and-exit "$file"
