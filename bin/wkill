#!/bin/sh -eu

# Copyright (C) 2023  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Select a window in sway and kill associated process. Like xkill.
#
# deps: sway, jq, slurp, procps
###

swaymsg -t get_tree | jq -r "\
	.. | \
	select(.pid? and .visible?) | \
	.rect + {pid: .pid} | \
	\"\(.x),\(.y) \(.width)x\(.height) \(.pid)\" \
	" | slurp -f'%l' | xargs kill "$@"
