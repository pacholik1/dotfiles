#!/bin/sh -eu

# Copyright (C) 2019  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Convert a video to mkv, with codecs preserved.
#
# deps: ffmpeg, coreutils
###

for i in "$@"; do
	[ -e "$i" ] || continue
	# ffmpeg -i "$i" -c:v copy -c:a copy -c:s copy "${i%.*}.mkv"
	ffmpeg -i "$i" -c copy "${i%.*}.mkv"
done
