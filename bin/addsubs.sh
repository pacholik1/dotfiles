#!/bin/sh -eu

# Copyright (C) 2021  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Add subtitles to video
#
# deps: iconv, ffmpeg, moreutils
###

v="$1"
s="$2"

if isutf8 --quiet "$s"; then
	cat "$s"
else 
	iconv -f windows-1250 "$s"
fi | ffmpeg \
	-fflags +genpts \
	-i "$v" -i - \
	-map 0:0 -map 0:1 -map 1:0 \
	-metadata:s:s:0 language=eng \
	-c copy -c:s srt \
	"${v%.*}_sub.mkv"
