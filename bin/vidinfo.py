#!/usr/bin/python3

# Copyright (C) 2022  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# deps: pymediainfo
###

import sys
from pymediainfo import MediaInfo
from datetime import timedelta


def main(filename):
    media_info = MediaInfo.parse(filename)
    general = media_info.general_tracks[0]

    print(general.file_name)
    print("Duration", timedelta(milliseconds=general.duration))
    print("Audio langs:", general.audio_language_list)
    print("Subtitle langs:", general.text_language_list)


if __name__ == '__main__':
    main(*sys.argv[1:])
