#!/usr/bin/python3

# Copyright (C) 2023  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import sys
import re
import functools
import calendar
from datetime import date, timedelta
from itertools import zip_longest
from workalendar.europe import CzechRepublic
from typing import Iterable

###
# This script generates a calendar with czech holidays for the previous, current and next months.
#
# deps: workalendar
###

REVERSED = '\x1b[7m'
RED = '\x1b[31m'
RESET = '\x1b[0m'


def _cast_args(f):
    @functools.wraps(f)
    def wrapped(*args):
        typed_args = (tpe(val) for tpe, val in
                      zip(f.__annotations__.values(), args))
        return f(*typed_args)
    return wrapped


class CalendarWithHolidays():
    def __init__(self, today: date = date.today(), year: int = 0, month: int = 0):
        self.today = today
        self.year = year or today.year
        self.month = month or today.month
        self.months_before = 1
        self.months_after = 1
        self.cz_holidays = CzechRepublic().holidays

    @staticmethod
    def day_to_pattern_repl(day: int) -> tuple[str, str]:
        if day < 10:
            return rf' {day}(?!\d)', f' {day}'          # I’d have used \b instead of lookaheads,
        else:                                           # but there are also escape sequences if
            return rf'(?<!\d){day}(?!\d)', f'{day}'     # it’s a holiday today

    def one_month_calendar(self, year: int, month: int) -> str:
        cal = calendar.month(year, month)
        cal = '\n'.join(line.ljust(22) for line in cal.splitlines())
        holidays_this_month = [
            date for date in dict(self.cz_holidays(year=year)) if date.month == month]

        for holiday in holidays_this_month:                         # highlight national holidays
            pattern, repl = self.day_to_pattern_repl(holiday.day)
            cal = re.sub(pattern, f"{RED}{repl}{RESET}", cal)

        if year == self.today.year and month == self.today.month:   # highlight this day
            pattern, repl = self.day_to_pattern_repl(self.today.day)
            cal = re.sub(pattern, f"{REVERSED}{repl}{RESET}", cal)
        return cal

    @staticmethod
    def next_month(year: int, month: int, n: int) -> tuple[int, int]:
        if n == 0:
            return year, month

        next_month_day = date(year, month, 28 if n < 0 else 1) + timedelta(days=31*n)
        return next_month_day.year, next_month_day.month

    @staticmethod
    def join_multiline_strings(*strings: str, ljust: int = 22, gap: int = 0) -> Iterable[str]:
        for lines in zip_longest(*(s.splitlines() for s in strings), fillvalue=''):
            yield (' ' * gap).join(line.ljust(ljust) for line in lines)

    def print_calendar(self):
        calendars = [self.one_month_calendar(*self.next_month(self.year, self.month, n))
                     for n in range(-self.months_before, self.months_after + 1)]
        print('\n'.join(self.join_multiline_strings(*calendars)))


@_cast_args
def main(year: int = 0, month: int = 0):
    cal = CalendarWithHolidays(year=year, month=month)
    # cal = CalendarWithHolidays(today=date(2023, 11, 17), year=year, month=month)
    # cal = CalendarWithHolidays(today=date(2024, 1, 1), year=year, month=month)
    cal.print_calendar()


if __name__ == '__main__':
    main(*sys.argv[1:])
