#!/bin/sh -eu

# Copyright (C) 2021  Pachol, Vojtěch <pacholick@gmail.com>
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

###
# Merge audio and video
#
# deps: ffmpeg, mediainfo
###

if [ -n "$(mediainfo --Inform="Audio;%ID%" "$1")" ]; then 
	a="$1"
elif [ -n "$(mediainfo --Inform="Audio;%ID%" "$2")" ]; then 
	a="$2"
else
	exit 1
fi

if [ -n "$(mediainfo --Inform="Video;%ID%" "$1")" ]; then 
	v="$1"
elif [ -n "$(mediainfo --Inform="Video;%ID%" "$2")" ]; then 
	v="$2"
else
	exit 2
fi

output="${3:-merged.mkv}"

ffmpeg -i "$a" -i "$v" -c copy -map 0:a -map 1:v "$output"
