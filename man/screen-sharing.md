#install:

    xdg-desktop-portal
    xdg-desktop-portal-wlr

- and no other xdg-desktop-portal-*

#enable pipewire webrtc in Chrome:

    chrome://flags/#enable-webrtc-pipewire-capturer

#Chrome on wayland:

.var/app/com.google.Chrome/config/chrome-flags.conf

    --enable-features=UseOzonePlatform
    --ozone-platform=wayland

#still having problems?

https://github.com/emersion/xdg-desktop-portal-wlr/wiki/%22It-doesn't-work%22-Troubleshooting-Checklist
