install from man/installed

export gpg key

    gpg -a --output gpg-secret-key.asc --export-secret-keys pacholick@gmail.com

import gpg key

    gpg --import gpg-secret-key.asc
    gpg --edit-key 11223344
    gpg> trust  # and type 5 for ultimate trust

generate ssh key
key on raspberry
login to gitlab, enter key
clone dotfiles, pass, keyboards

    sudo cp sway@.service /etc/systemd/system/
    sudo systemctl enable sway@7

    cd ~/vcs/keyboards; ./rsync-them.sh

/tmp

    tmpfs	/tmp	tmpfs	defaults	0	0

get wallpaper ~/pics/apod/NGC1316Center_HubbleNobre_2585.jpg

    mkdir -p ~/pics/apod/
    mv NGC1316Center_HubbleNobre_2585 ~/pics/apod/
    ln -s ~/pics/apod/NGC1316Center_HubbleNobre_2585.jpg ~/pics/apod.jpg
    ln -s ~/pics/apod.jpg ~/pics/wallpaper.jpg

create ~/.megarc
get packages from mega

get powerline-fonts

    git clone git@github.com:powerline/fonts.git
    cd ~/vcs/fonts/
    ./install.sh

auto updates

    sudo dpkg-reconfigure -plow unattended-upgrades

update-alternatives

    sudo update-alternatives --config editor
    sudo update-alternatives --config vim
    sudo update-alternatives --config view
    sudo update-alternatives --config vimdiff

start using flatpak

    sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    flatpak install com.spotify.Client
    sudo flatpak remote-add --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
    flatpak install flathub-beta com.google.Chrome

set gruvbox theme in qt5ct
