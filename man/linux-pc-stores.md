| adress                          | cpu       | 14" 32G RAM |
|---------------------------------|-----------|-------------|
| https://laptopwithlinux.com     | intel     | €793        |
| https://puri.sm/products        | intel     | $1638       |
| https://slimbook.es             | intel/amd | €684        |
| https://starlabs.systems        | intel/amd | £1038       |
| https://system76.com            | intel/amd | $1578       |
| https://www.pine64.org          | arm       |             |
| https://www.tuxedocomputers.com | intel/amd | €954        |
| https://junocomputers.com       | intel     | £700        |
