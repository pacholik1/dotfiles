#certificate info

openssl x509 -in cer.pem -text


#Convert a DER file (.crt .cer .der) to PEM

openssl x509 -inform der -in certificate.cer -out certificate.pem


#Convert PFX to PEM

openssl pkcs12 -in 'Certifikát pro e-shop a Mikroelektroniku.pfx' -nodes
