    LOL
    ├── lol
    │   ├── __init__.py
    │   └── _version.py
    └── setup.py

###`setup.py` 

    #!/usr/bin/python3
    from setuptools import setup

    with open('lol/_version.py') as f:
        exec(f.read())

    setup(
        name='lol',
        version=__version__,    # noqa: F821
        packages=['lol'],
    )

###`lol/__init__.py` 

    # flake8: noqa: F401
    from ._version import __version__, __version_info__

###`lol/_version.py` 

    __version_info__ = (0, 1, 0)
    __version__ = '.'.join(str(i) for i in __version_info__)
