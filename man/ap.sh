#!/bin/bash
#
# COPYING: http://www.wtfpl.net/txt/copying
#
# When on wire, create wireless acces point
#
# uses create_ap https://github.com/oblique/create_ap
#
# you need to set environment variables $WIRED and $WIRELESS
#
# copy to /etc/wicd/scripts/postconnect/ap.sh


if [ "$1" == "wired" ]; then
	# fork with nohup, so wicd doesn't hang
	nohup "$0" fork > /tmp/create_ap.log 2>&1 &

elif [ "$1" == "fork" ]; then
	sleep 10	# wait a little, there may still occur some dns searching etc.
	exec create_ap --no-virt "$WIRELESS" "$WIRED" "$HOSTNAME" asdfasdf
fi

exit


#!/bin/sh
#
# COPYING: http://www.wtfpl.net/txt/copying
#
# Kill on disconnect, simple as that
#
# copy to /etc/wicd/scripts/predisconnect/ap.sh

killall create_ap
