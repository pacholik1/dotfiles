#cut

-ss has to be before -i, or at least in older versions

    ffmpeg -ss "$start" -i input.mp4 -c copy -t "$duration" output.mp4
    ffmpeg -ss "$start" -i input.mp4 -c copy -to "$end" output.mp4
    ffmpeg -ss "$start" -i input.mp4 -frames:v "$num_frames" -vcodec copy output.mp4

one inute clip starting from 8:00

    ffmpeg -ss 00:08:00 -i input.mp4 -ss 00:01:00 -t 00:01:00 -c copy input.mp4


#crop

    ffmpeg -i input.mp4 -vf "crop=$w:$h:$x:$y" -c:a copy output.mp4


#extract subtitles

    ffmpeg -i Movie.mkv -map 0:s:0 subs.srt
