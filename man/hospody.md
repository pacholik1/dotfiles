| Marina      | ~100 m |
| Šenk        | 161 m  |
| Pošta       | 206 m  |
| Anh Dung    | 206 m  |
| Vatikán     | 213 m  |
| Sale e Pepe | 241 m  |
| Casablanca  | 253 m  |
| Štika       | 260 m  |
| Šotek       | 271 m  |
| Rio         | 310 m  |
| Rybárna     | 415 m  |
| Ponorka     | 423 m  |
| Záložna     | 427 m  |
| Namaste     | 434 m  |
| Trinity     | 440 m  |
| Coco        | 456 m  |
| Koníček     | 462 m  |
| Pavouk      | 469 m  |
| Bohemka     | 478 m  |
| Bushman     | 480 m  |
| Vinothéka   | 481 m  |
| Bakla       | 485 m  |
| Che’s       | 488 m  |
| Pípa        | 491 m  |
| U Dušičků   | 496 m  |
| 29          | 498 m  |
|-------------|--------|
| Beseda      | 517 m  |
| Laputa      | 524 m  |
| Kocouř      | 537 m  |
