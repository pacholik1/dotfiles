# Beware, this file will be read and executed in setup.py.

__version_info__ = (0, 0, 1)
__version__ = '.'.join(str(i) for i in __version_info__)
