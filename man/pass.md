https://manuel-ohlendorf.de/posts/009_sharing_secrets_with_pass/

I don’t like password managers. I don’t trust them. But when you work in a small team and use a lot of different servers and technologies you sometimes have to securely store and share a secret with them. There are a lot of password managers out there that can do that but I want a very simple command line tool that just uses standard technologies and — Tata — Pass, the standard unix password manager hits the spot.

The documentation is quite good and there are some helpful blogpost on how to setup pass. But what I am missing is how to share the stored and encrypted passwords.

Setup
To setup pass, you need a GnuPG key which pass uses to encrypt all your secrets. Run the command and follow the instructions:

$ gpg --gen-key
You should stick to the defaults recommended by GnuPG. Only for the key length I would choose 4096 bits instead of just 2048 bits because you can and bigger is better :-)

After GnuPG has generated the key it prints the details to the key. One line should look something like:

pub   4096R/11223344 2009-07-17
You need to remember (copy into clipboard) the 8 character key id of the public key. Here it is 11223344.

Now you can initialize your pass password store with it:

$ pass init 11223344
With pass you can use git to track changes made to the password store and if you have a remote repository, you can also share the store with others. Let’s initialize the git repository with pass:

$ pass git init
And add the remote URL:

$ pass git remote add origin kexec.com:pass-store
Since pass just uses git you don’t have to use pass for managing the repository. You can also just use the bare git command for that. The default path for the password store is ~/.password-store/.

Sharing
Now that you have a running pass setup you can share that with others. For decrypting the secrets you have to export and share the public and private GnuPG key that you just created.

Keep in mind that anyone who has access to the key and knows the password of the key is able to decrypt all your passwords.

$ gpg -a --output gpg-secret-key.asc --export-secret-keys 11223344
If you want to provide a team member access to the password store she needs the exported key in the file gpg-secret-key.asc, access to the git remote repository holding the store and the password of the GnuPG key, of course.

This is what the colleague has to do after installing pass:

Import the key into her GnuPG installation:

$ gpg --import gpg-secret-key.asc
Increase the trust-level to maximum with the GnuPG editor:

$ gpg --edit-key 11223344
Now, that you are in the interactive editor you have to enter the following:

gpg> trust
and type 5 for ultimate trust, then confirm with y and enter quit to exit. The final step is to clone the git repository:

$ git clone kexec.com:pass-store ~/.password-store/
Your colleague is now able to decrypt all passwords and add new ones.

Security Considerations
Sharing the pass password store has some security issues that you should be aware of:

be careful when giving the public/private GnuPG key and password away. In the wrong hands, everything is lost. You shouldn’t use E-Mail for that. A USB-Drive that is securely wiped clear after sharing the key should do the trick.
you should also keep the git repository hidden and secure. Pass uses simple files to store the encrypted passwords and the filenames are clear. An attacker with access to the git repository is able to see the clear names of your encrypted passwords and might drew some dangerous conclusions from that.
Please keep that in mind and don’t blame me later ;-)

