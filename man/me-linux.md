**distro:**

~1/2009		Ubuntu
~11/2009	Xubuntu
8/2010		Debian

**de:**

~1/2009	  GNOME 2
~11/2009  Xfce
~6/2014   Awesome
3/2020    i3
4/2020    Sway

**browser:**

2004      Firefox
2006      Opera 
2013      Firefox 
2018      qutebrowser 

**editor:**

~2013     vim
4/2020    neovim-qt
