# If not running interactively, don't do anything
[ -z "$PS1" ] && return

HISTCONTROL=ignoreboth

export LANG=C.UTF-8

# **
shopt -s globstar
# don't override history file
shopt -s histappend
# update $LINES and $COLUMNS after each command
shopt -s checkwinsize
# no * in arglist if no match
# shopt -s nullglob

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# autologout tty
[[ $(tty) =~ /dev/tty[1-6] ]] && TMOUT=600

# shellcheck disable=1091
# source /etc/profile.d/undistract-me.sh

# _x='\e[0m\]'
# _W='\e[1m\]'
# _g='\e[37m\]'
prompt_command() {
	# shellcheck disable=SC2181
	if [ $? -eq 0 ]; then
		local DOLLAR=\$
	else
		local DOLLAR=!
	fi
	local x='\[\033[0m\]'
	local WHITE='\[\033[1m\]'
	# local black='\[\033[30m\]'
	local red='\[\033[31m\]'
	# local green='\[\033[32m\]'
	# local yellow='\[\033[33m\]'
	# local blue='\[\033[34m\]'
	# local magenta='\[\033[35m\]'
	# local cyan='\[\033[36m\]'
	local lgray='\[\033[37m\]'
	# local gray='\[\033[90m\]'
	# local lred='\[\033[91m\]'
	# local lgreen='\[\033[92m\]'
	# local lyellow='\[\033[93m\]'
	# local lblue='\[\033[94m\]'
	# local lmagenta='\[\033[95m\]'
	# local lcyan='\[\033[96m\]'
	# local white='\[\033[97m\]'

	local branch
	branch=$(git branch --show-current 2>/dev/null || true)
	[ -n "$branch" ] && branch="$branch"

	PS1="$WHITE\\u@\\h$x:$lgray\\w$x$red$branch$x$WHITE$DOLLAR $x"
}
# PROMPT_COMMAND="${PROMPT_COMMAND};prompt_command"
PROMPT_COMMAND="prompt_command;${PROMPT_COMMAND}"

#if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
#    . /etc/bash_completion
#fi
if [ -f /usr/share/bash-completion/bash_completion ] && ! shopt -oq posix; then
	# shellcheck disable=SC1091
	. /usr/share/bash-completion/bash_completion
fi
# MY_BASH_COMPLETION_COMPAT_DIR=$HOME/bin/bash_completion.d
for i in "$HOME"/bin/bash_completion.d/*; do
	# shellcheck disable=SC1090
	. "$i"
done
[ "$(type -p pandoc)" ] && eval "$(pandoc --bash-completion)"
[ "$(type -p kitty)" ] && eval "$(kitty + complete setup bash)"

if [ -f /usr/share/bash-completion/completions/fzf ]; then
	# shellcheck disable=SC1091
	. /usr/share/bash-completion/completions/fzf
	_fzf_setup_completion path qvim
	_fzf_setup_completion path rifle
	_fzf_setup_completion dir ranger

	# Custom fuzzy completion for passclip
	_fzf_complete_pass() {
		FZF_COMPLETION_TRIGGER='' _fzf_complete +m -- "$@" < <(
			passdir="${PASSWORD_STORE_DIR:-$HOME/.password-store}"
			find "$passdir" -iname '*.gpg' -printf '%P\n' | sed 's/\.gpg$//'
			# printf '%s\n' init ls find show grep insert edit \
			# 	generate rm mv cp git help version
		)
	}
	complete -F _fzf_complete_pass -o default -o bashdefault passclip

fi

eval "$(dircolors -b)"


# ALIASES
[ "$(type -p exa)" ] && alias ls='exa --group-directories-first --color=auto'
alias ll='ls -lF'
alias grep='grep --color=auto --perl-regexp --line-number'
alias curl='curl --silent'
# alias cal='ncal -bM'
alias cal='cal.py'
alias e='vim'     # for `bvim`
alias cd..='cd ..'

alias hcat='highlight -O xterm256'
alias icat='kitty +kitten icat'
alias anticensore='sed y/aeopcyxABEKMHOPCTX/аеорсухАВЕКМНОРСТХ/'
alias rot13='tr A-Za-z N-ZA-Mn-za-m'
alias qr='ttyqr -b'
alias curdate='date +%F'
alias unzip-cp852='~/bin/crap/unzip -O CP852'
alias create-tar='tar --create --auto-compress --file'
alias extract-tar='tar --extract --auto-compress --file'
alias list-movies='ls | sed -E "s/([^(]*) \(([0-9]*)\)/\2:\t\1/" | sort -n'
alias colorpicker='grim -g "$(slurp -p)" -t ppm - | convert - -format "%[pixel:p{0,0}]" txt:-'
alias video-length='mediainfo --Output="General;%Duration/String%"'

alias ap='sudo create_ap --no-virt "$WIRELESS" "$WIRED" "$HOSTNAME" asdfasdf'
alias lock='swaylock --color=282828 --daemonize; systemctl suspend'
alias NO_PUBKEY='sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys'

alias tmp='cd /tmp'
alias py='if jobs "%ipython3"; then fg "%ipython3"; else ipython3; fi'
# alias ptp='if jobs "%ptpython"; then fg "%ptpython"; else ptpython; fi'
alias js='if jobs "%node"; then fg "%node"; else node; fi'
alias ts='if jobs "%ts-node"; then fg "%ts-node"; else ts-node; fi'

alias xperiasend='kdeconnect-cli --name xperia --share'

alias rpimount='sshfs rpi.home:/mnt/wd/ /mnt/rpi/'
alias check-sda1='pumount /dev/sda1 && sudo fsck -y /dev/sda1; pmount /dev/sda1'

alias trans='trans -join-sentence -brief'
alias fde='trans de:'
alias fes='trans es:'
alias fuk='trans uk:'
alias fda='trans da:'
alias tcs='trans :cs'
alias tde='trans :de'
alias tes='trans :es'
alias tuk='trans :uk'
alias tda='trans :da'

alias cs='swaymsg input type:keyboard xkb_layout uscz'
alias сы='swaymsg input type:keyboard xkb_layout uscz'
alias цс='swaymsg input type:keyboard xkb_layout uscz'
alias sk='swaymsg input type:keyboard xkb_layout ussk'
alias de='swaymsg input type:keyboard xkb_layout usde'
alias es='swaymsg input type:keyboard xkb_layout uses'
alias fr='swaymsg input type:keyboard xkb_layout usfr'
alias ru='swaymsg input type:keyboard xkb_layout ruus'
alias pl='swaymsg input type:keyboard xkb_layout uspl'
alias boxdrawing='swaymsg input type:keyboard xkb_layout boxdrawing'
alias flags='swaymsg input type:keyboard xkb_layout flags'

# alias eshop='cd ~/work/mel/eshop'
# alias plz-send-artifacts='rsync -ah --progress -vze ssh fareoneshop*.tgz install_*.sh plz:~'
# alias frontend-incver='incversion.sh /VERSION/ ~/work/mel/eshop/frontend/src/app/version.js'
# alias backend-incver='incversion.sh /__version__/ ~/work/mel/eshop/backend/foebackend/__init__.py'
# alias workers-incver='incversion.sh /version/ ~/work/mel/eshop/workers/setup.py'

alias bynaps='cd ~/dev/fantastic-bynaps/'

alias dcb='docker-compose build'
alias dcu='docker-compose up -d'
alias dcd='docker-compose down'
alias dcl='docker-compose logs -f'
alias dcul='dcu && dcl'
alias dcdu='dcd && dcu'
alias dcbul='dcb && dcu && dcl'
alias dcdul='dcd && dcu && dcl'
alias dcbdul='dcb && dcd && dcu && dcl'

alias vue='node_modules/.bin/vue'
# alias yarn='node_modules/.bin/yarn'
alias cf='contentful'

# BINDS
# free to bind: oqvxyz
# edit command in vim with [Insert]
bind '"\e[2~":edit-and-execute-command'
# no beep
bind 'set bell-style none'
bind '"\C-o":"ranger-cd\C-m"'
# shellcheck disable=1091
if [ -f /usr/share/doc/fzf/examples/key-bindings.bash ]; then
	source /usr/share/doc/fzf/examples/key-bindings.bash	# debian
elif [ -f /usr/share/fzf/shell/key-bindings.bash ]; then
	source /usr/share/fzf/shell/key-bindings.bash		# fedora
elif [ -f /usr/share/fzf/key-bindings.bash ]; then
	source /usr/share/fzf/key-bindings.bash			# arch
fi
# bind '"\C-x":"rifle $(fzf)\C-m"'
eval "$(thefuck --alias)"
bind '"\C-g":"fuck\C-m"'

# FUNCTIONS

n() {
	nohup "$@" &> /dev/null &
}

rpisend() {
	rsync -ah --progress -vze ssh "$@" rpi.home:/mnt/wd/downloads/
}

M() {
	aptitude search "!~M~i$*"
}

dvb() {
	mpv --deinterlace=yes "dvb://$*"
}

ranger-cd() {
    tmp="$(mktemp)"
    ranger --choosedir="$tmp" "$@"
    if [ -f "$tmp" ]; then
		cd -- "$(cat "$tmp")" || true
	fi
    rm -f -- "$tmp"
}

winpython() {
	PYTHONIOENCODING=utf8 \
	WINEDEBUG=-all \
	wine64 'C:\python3\python.exe' "$@"		# 2>/dev/null
}

c() {
	if [ "$#" -eq 0 ]; then
		cd ~/work/20PDN03_PublicisDenmark_Connect/ || true
	else
		~/bin/connect "$@"
	fi
}

v() {
	if [ "$#" -eq 0 ]; then
		cd ~/work/veeva-alk/ || true
	else
		~/bin/veeva "$@"
	fi
}

cdr() {
	dir="$(sed -En "/^${1::1}:/s///p" "$HOME/.local/share/ranger/bookmarks")"
	cd "$dir" || return
}

# free ctrl combinations:
# ^G (abort search)
# ^Q (resume output)
# ^S (stop output, search forward)
# ^T (transpose chars)
# ^Y (paste)

export NVM_DIR="$HOME/.nvm"
# shellcheck disable=1091
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
# shellcheck disable=1091
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
